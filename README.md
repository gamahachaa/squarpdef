# NON OFFICIAL squarp© definition file manager
## Main functionalities
- Create Hapax definition files.
    - Validates values based on hapax default definiton.
    - Keyboard selector for drum notes
- Load your definitions (hapax or pyramid) to edit and save them.
- Browse existing definitions shared by the comunity
    - Avialable as git repo [](https://codeberg.org/gamahachaa/pages).
- Print an abstract of the defition (for your studio archiving).
- Cross platform 
    - only browser based for now but soon windows, mac and linux desktop. (you can git pull the code and compile to your platform if you will)
    - mobile is feasible but trick due to data table display.
- Midi virtual keyboard (todo)
    - to hear drumlane selected notes.


### Overview 
![](_pics/main_view.png)


![](_pics/menu_desc.png)

### File management

#### Open
![](_pics/open.png)

#### Browse

![](_pics/browse.png)

#### convert
![](_pics/converter.png)

#### print
![](_pics/print.png)

### Values validation

#### track
![](_pics/track.png)

#### Pot assigns
![](_pics/pots.png)

#### Automations
![](_pics/automations.png)

#### Program changes
![](_pics/pc.png)

#### Drum lanes
![](_pics/drumlanes.png)

#### Controls (cc, cc pairs, nrpns)

![](_pics/controls.png)

#### Transfering Controls (CC, NRPN)

You can now fetch a Control Change from your list of CC or NRPNs
and inject them in the Pot or Automnations of you choice.

Either by pulling or pushing.
** Pulling **
![](pull_pot.png)
![](pull_auto.png)
** Pushing ** 
![](push.png)
![](push_auto_result.png)

## How to compile
### Haxe
The code is written in Haxe.org.
If you never used it, you will learn it in no time if you are familliar with Java, C#, js (any C inspired code syntax).
The main advantage of Haxe is :
- That it can compile to a lot of platforms.
- It has type inference (so very very less debuging compare to dynamic code like js).
- It compiles lightnig fast.
- Tons of additional libraries (like cross platform midi).
- and more...

Code is beta, 
poorly documented (i will add more but most of the things are quite straigthforward),
Some refactoring can be done for better encapsulation...
Any contribution is welcome.

### UI 
the UI uses haxeui.org.
again here for it cross platform compile but aslso for XML structure definitions.

-lib haxeui-core
-lib haxeui-html5
(get them from git)

### Other dependencies

-lib signals
(because i like signal events...)

-lib uglifyjs
(optional just to minify the js output)