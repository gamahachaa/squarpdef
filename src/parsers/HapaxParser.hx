package parsers;

import haxe.Exception;
import haxe.ui.notifications.NotificationManager;
import haxe.ui.notifications.NotificationType;
import models.Definition;
import models.HapaxDefinition;
import types.*;
import types.HapaxEnums;
import types.LowRange;
import types.MidiNote;
import types.PotNumber;

using StringTools;
using utils.StringToolsBB;

//
class HapaxParser extends BaseParser {
	var _def:HapaxDefinition;

	public function new(file:String) {
		super(file);
	}

	override public function parse(def:Definition) {
		super.parse(def);
		def.stringProperties = [];
		if (_class != HapaxDefinition)
			throw new Exception('Wrong def, should be instance of $_class');
		_def = cast(def, HapaxDefinition);
		var propRegMap:Map<String, EReg> = [
			HapaxDefinition.VERSION => Regex.version,
			HapaxDefinition.TRACKNAME => Regex.trackName,
			HapaxDefinition.OUTCHAN => Regex.trackName,
		];
		// var lines = _file.split(BaseParser.BR);
		var i = 0;
		var line = "";
		var tag = "";
		var tagOpened = false;
		NotificationManager.instance.clearNotifications();
		try {
			while (i < lines.length) {
				/**
					@todo store and manage line comments
				**/
				line = cleanLine(lines[i]); // Remove trailling spaces and line comments
				// trace(line);
				if (line != '' && !Regex.commentStartLineEreg.match(line)) {
					if (line == "")
						continue;
					if (Regex.basicValueStartEreg.match(line)) {
						var prop = Regex.basicValueStartEreg.matched(1);
						var value = Regex.basicValueStartEreg.matched(2);
						switch (prop) {
							case HapaxDefinition.INPORT:
								_def.inport = if (HapaxEnums.Inport.contains(value)) value else throw 'Unknow INPORT : $value';
							//                            case HapaxDefinition.INPORT: _def.inport = (Type.createEnum(Inport, value));
							case HapaxDefinition.INCHAN:
								_def.inchan = (new Channel(value.stripLeadingZeros()));
							case HapaxDefinition.OUTPORT:
								_def.outport = if (HapaxEnums.Outport.contains(value)) value else throw 'Unknow OUTPORT : $value';
							case HapaxDefinition.OUTCHAN:
								_def.outchan = (new Channel(value.stripLeadingZeros()));
							case HapaxDefinition.MAXRATE:
								_def.maxrate = (new Rate(value));
							case HapaxDefinition.TYPE:
								_def.trackType = if (HapaxEnums.TrackType.contains(value)) value else throw 'Unknow TRACK TYPE : $value';
							case HapaxDefinition.VERSION:
								_def._version = value;
							case HapaxDefinition.TRACKNAME:
								_def.trackName = new Name(value);
							case _:
								trace('LINE ${i + 1}: Unkown property $prop on line $line ');
						}
					} else if (Regex.openTagEreg.match(line)) {
						if (tagOpened)
							throw 'New opened tag $line while $tag is not close yet';
						tag = Regex.openTagEreg.matched(1);
						tagOpened = true;
					} else if (Regex.closeTagEreg.match(line)) {
						if (!tagOpened)
							throw 'New closing tag $line while no open tag for the same yet';
						tag = "";
						tagOpened = false;
					} else if (tag != "") {
						switch (tag) {
							case DrumLane.TAG:
								parseDrumLane(line);
							case HapaxDefinition.COMMENT:
								_def.comment += '$line\n';
							case ProgramChange.TAG:
								parseProgramChanges(line);
							case ControlChange.TAG:
								parseControlChange(line);
							case ControlChangePair.TAG:
								parseControlChangePair(line);
							case Nrpn.TAG:
								parseNrpn(line);
							case PotAssign.TAG:
								parseAssign(line);
							case Automation.TAG:
								parseAutomation(line);
							case _:
								trace(tag);
						}
					} else {
						throw new haxe.Exception('LINE ${i + 1}: Unkown line syntax "$line" ');
					}
				}
				i++;
			}
			NotificationManager.instance.addNotification({
				title: 'File parsing ALL GOOD !',
				body: 'No error found\nThe definition file is loaded',
				expiryMs: 2000
			});
		} catch (e) {
			NotificationManager.instance.addNotification({
				title: 'Parsing error LINE ${i + 1}:',
				body: 'line ${i + 1}. "${e.message}"\ncontent: "$line" !',
				type: NotificationType.Error,
				expiryMs: -1
			});

			// trace('Parsing error LINE ${i+1}: "${e.message}" content: "$line" !');
		}
	}

	function parseAutomation(s:String) {
		// trace(s);
		var reg = Regex.automationsValues;
		var matched = reg.match(s.trim());

		var potType = Type.createEnum(Pot, reg.matched(1));

		var value = reg.matched(2);

		var def = reg.matched(9) ?? "";

		////trace(potType, value, def);
		_def.addAutomation(new Automation(potType, value, def));
	}

	function parseAssign(s:String) {
		// trace(s);
		var reg = Regex.potAssignValues;
		var matched = reg.match(s);
		var potNum = reg.matched(1);
		var val = reg.matched(3);
		var def = reg.matched(5);
		// trace(def);
		var potType = Type.createEnum(Pot, reg.matched(2));
		var pot = new PotAssign(new PotNumber(potNum), potType, val, def);
		// trace("yo");
		_def.addPot(pot);
	}

	function parseDrumLane(s:String) {
		var reg = Regex.drumlaneReg;
		if (reg.match(s)) {
			var name = reg.matched(16);
			var row = reg.matched(1);
			var trg = reg.matched(2);
			var chn = reg.matched(6);
			var note = reg.matched(11);
			// var dl = new DrumLane(Std.parseInt(values[0]), new LowRange(values[1]), new Channel(values[2].stripLeadingZeros()), new MidiNote(values[3]),new Name(spli[1], false));
			var dl = new DrumLane(Std.parseInt(row),new LowRange(trg),new Channel(chn.stripLeadingZeros()),new MidiNote(note), new Name(name, false));
			_def.addDrumLane(dl);
		} else {
			throw "INVALID DRUMLANE FORMAT";
		}
		
	}

	function parseProgramChanges(s:String) {
		var reg = Regex.pcValuesReg;
		if (reg.match(s)) {
			var name = new Name(reg.matched(6), false);
			var values = reg.matched(1);
			var vals = if (values.indexOf(":") > -1) values.split(":") else [values];
			var pc = new HighRange(vals[0]);
			var msb = new LowRange(vals.length > 1 ? vals[1] : "NULL");
			var lsb = new LowRange(vals.length > 2 ? vals[2] : "NULL");
			_def.addProgramChange(new ProgramChange(name, pc, msb, lsb));
		} else {
			if (Regex.pcValues.match(s))
				throw 'INVALID program change. A Program change MUST have a name';
			else
				throw 'INVALID program change.';
		}
	}

	function parseControlChange(s:String) {
		// trace(s);
		var spli = s.split(" ");
		var numberDefault = spli.shift();
		var reg = Regex.ccValues;
		reg.match(numberDefault);
		var name = new Name(spli.join(" "), false);
		var cc = new LowRange(reg.matched(1));
		var defo = reg.matched(3) ?? "";
		_def.addContrlChange(new ControlChange(name, cc, defo));
	}

	function parseControlChangePair(s:String) {
		trace(s);
		var spli = s.split(" ");
		var numberDefault = spli.shift();
		var reg = Regex.ccPairsValues;
		reg.match(numberDefault);
		var name = new Name(spli.join(" "), false);

		var msb = new LowRange(reg.matched(1));

		var lsb = new LowRange(reg.matched(2));

		var defo = reg.matched(4) ?? "";
		trace(name, msb, lsb, defo);
		var ccp = new ControlChangePair(name, msb, lsb, defo);
		trace(ccp.id);
		_def.addContrlChangePair(ccp);
	}

	function parseNrpn(s:String) {
		var reg = Regex.nrpnValuesName;
		if (reg.match(s)) {
			var depthIs7 = reg.matched(4) == "7";
			var tmpLsb = Std.parseInt(reg.matched(3));
			var tmpMsb = Std.parseInt(reg.matched(2));
			// trace(s, reg.matched(2), reg.matched(2)== null || reg.matched(2).trim()=="");
			var msb = new LowRange((reg.matched(2) == null || reg.matched(2).trim() == "") ? "0" : reg.matched(2));
			if (depthIs7 && (tmpLsb > 127 || tmpMsb > 127)) {
				throw '\nDepth is 7bits but MSB is ${reg.matched(2)} or LSB ${reg.matched(3)} \nShould not be over 127';
			} else if (!depthIs7 && (tmpLsb > 127 && msb != "0")) {
				'\nDepth is ${reg.matched(4)} and LSB=${reg.matched(3)}, fine but MSB=${reg.matched(2)} should be 0 or ommited ("")';
			}
			// var msb = new LowRange(reg.matched(2));
			_def.addNrpn(new Nrpn(new Name(reg.matched(8), false), !depthIs7,
				depthIs7 ? Right(new LowRange(reg.matched(3))) : Left(new Binary128(reg.matched(3))), msb, reg.matched(6) ?? ""));
		} else
			throw '$s doesn\'t match NRPN rules.';
	}
}
