package parsers;
import models.Definition;

using StringTools;
class BaseParser {
    public static var BR = "\n";
    var _file:String;
    var lines:Array<String>;
    var _class:Class<Definition>;
    public function new(file:String) {
        this._file = file;
        this.lines = [];
    }
    public function parse(def:Definition){
        _class = Type.getClass(def);
        lines = _file.split(BaseParser.BR);
    }
    public function cleanLine(s:String):String{
        s = if(s.indexOf("#")>-1) s.substr(0,s.indexOf("#")) else s;// just use the bit before comments
        return s.trim();
    }
}
