package parsers;

import haxe.Exception;
import models.Definition;
import models.PyramidDef;
import types.Regex;

class PyramidParser extends BaseParser {
	public function new(file:String) {
		super(file);
	}

	override public function parse(def:Definition) {
		super.parse(def);
		def.stringProperties = [];
		if (_class != PyramidDef)
			throw new Exception('Wrong def, should be instance of $_class');
		var _def:PyramidDef = cast(def, PyramidDef);
		try {
			var i = 0;
			var line = "";
			while (i < lines.length) {
				line = cleanLine(lines[i]);
				if (line == "")
					break;
				if (Regex.pyraKeyValue.match(line)) {
					_def.stringProperties.set(Regex.pyraKeyValue.matched(1), Regex.pyraKeyValue.matched(1) + ":" + Regex.pyraKeyValue.matched(2));
				} else
					trace("no match " + line);
				i++;
			}
			_def.checkSum();
		} catch (e:haxe.Exception) {
			trace(e);
		}
	}
}
