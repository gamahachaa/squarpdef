package controllers;

import haxe.Exception;
import haxe.ui.containers.dialogs.Dialogs.SelectedFileInfo;
import models.Definition;
import models.HapaxDefinition;
import models.PyramidDef;
import parsers.HapaxParser;
import parsers.PyramidParser;
import types.Name;
import utils.DefinitionIdentifier;
import views.Dialoging;
import views.HapaxEditorView;

using StringTools;
#if js
import js.Browser;
#end
class HapaxEditorController {
	public var view:HapaxEditorView;

	public function new() {
		view = new HapaxEditorView();
		view.saveSignal.add(onSave);
		view.fileLoaded.add(onFileLoaded);
		view.pasted.add(onPaste);
		view.printSignal.add(doPrintJob);

	}

	function onSave(?filename:String = "", ?hapax:Bool = true) {
		// trace(def);
		var def = makeDefinition();
		var f = filename == "" ? def.trackName : filename;
		f += !hapax ? "_pyra.txt" : ".txt";
		 trace(f);
		try {
			view.doSaveFile(f, hapax ? def.toTemplate(view.addDoc) : def.toPyramid().toTemplate());
		} catch (e:Exception) {
			// trace(e.message);
			Dialoging.showError("Error", e.message + "\n" + e.stack);
		}
	}

	function onPaste(s:String) {
		checkAndParse(s);
	}

	function onFileLoaded(selected:SelectedFileInfo) {
		try {
			final fileName = selected.name;//@todo something with file name...
			final content:String = selected.isBinary ? selected.bytes.toString() : selected.text;
			checkAndParse(content, fileName);
		} catch (e:Exception) {
			Dialoging.showError("Error", e.message);
		}
	}

	function checkAndParse(s:String, ?filename="") {
		var hapaxDef:HapaxDefinition = new HapaxDefinition();
		var _class:Class<Definition> = DefinitionIdentifier.identify(s);
		try {
			if (_class == HapaxDefinition) {
				final parser:HapaxParser = new HapaxParser(s);
				parser.parse(hapaxDef);
			} else if (_class == PyramidDef) {
				try {
					// trace("Pyra", s);
					final pyradef = new PyramidDef();
					final parser:PyramidParser = new PyramidParser(s);
					parser.parse(pyradef);
					hapaxDef = pyradef.toHapax();

				} catch (e) {
					trace(e.message);
				}
			} else {
				//
				throw new Exception("Not an Hapax nor a Pyramid definition file");
			}
			view.populate(hapaxDef);
			if(filename.indexOf("__") > -1){
				final split = filename.split("__");
				view.setUpGoogleButton( split[1], split[0]);
			}else if(filename!=""){
				final noExt = filename.split(".")[0];
				view.setUpGoogleButton(noExt );
			}else if(hapaxDef.trackName.trim()!=""){
				view.setUpGoogleButton(hapaxDef.trackName );
			}
		} catch (e:Exception) {
			throw e.message;
		}
	}
	function makeDefinition():HapaxDefinition {
		trace("make def");
		var d = new HapaxDefinition();
		var trckName =(this.view.TRACKNAME.text == "" || this.view.TRACKNAME.text == null) ? "NULL" : this.view.TRACKNAME.text;
		d.trackName = new Name(trckName);
		d.trackType = this.view.TYPE.value;
		d.outchan = this.view.OUTCHAN.value;
		d.inport = this.view.INPORT.value;
		d.inchan = this.view.INCHAN.value;
		d.outport = this.view.OUTPORT.value;
		d.comment = this.view.COMMENT.text ?? "";
		d.maxrate = this.view.winAuto.getMaxRate();
		d.drumlanne = this.view.winDl.data;
		d.cc = this.view.winControls.data.ccs == [] ? null : this.view.winControls.data.ccs;
		d.ccp = this.view.winControls.data.ccps == [] ? null : this.view.winControls.data.ccps;
		d.nrpn = this.view.winControls.data.nrpn == [] ? null : this.view.winControls.data.nrpn;
		d.pc = this.view.winPc.data == [] ? null : this.view.winPc.data;
		// seperate for control men add
		d.potAssign = this.view.winPots.data == [] ? null : this.view.winPots.data;
		d.automation = this.view.winAuto.data;
		
		return d;
	}
	// function updateCC() {
	// 	d.cc = this.view.winControls.data.ccs == [] ? null : this.view.winControls.data.ccs;
	// 	d.ccp = this.view.winControls.data.ccps == [] ? null : this.view.winControls.data.ccps;
	// 	d.nrpn = this.view.winControls.data.nrpn == [] ? null : this.view.winControls.data.nrpn;
	// }
	#if js
	function doPrintJob() {
		try{
			var def = makeDefinition();
			var winB = Browser.window.open();
			winB.document.write(def.toTemplate(view.addDoc, true));
			winB.print();
			winB.close();
		}catch(e){
trace(e.message);
		}
	}
	#end
}
