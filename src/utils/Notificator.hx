package utils;

import haxe.ui.notifications.NotificationManager;
import haxe.ui.notifications.NotificationType;

class Notificator{
    public static function dispatchNoErorNotification(text:String){
		NotificationManager.instance.addNotification({
			title: '${text} : All good',
			body: 'No error found',
			expiryMs: 2000
		});
	}
	public static function dispatchErrorNotif(text:String, title:String, body:String){
		
		NotificationManager.instance.addNotification({
			title: '${text} : $title bad value !',
			body: body,
			type: NotificationType.Error,
			expiryMs: -1
		});
	}
	public static function dispatchWarnNotif(title:String, body:String){
		
		NotificationManager.instance.addNotification({
			title: title,
			body: body,
			type: NotificationType.Warning,
			expiryMs: -1
		});
	}
	public static function dispatchSuccessNotif(title:String, body:String){
		
		NotificationManager.instance.addNotification({
			title: title,
			body: body,
			type: NotificationType.Success,
			expiryMs: 3000
		});
	}
}