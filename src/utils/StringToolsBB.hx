package utils;
using StringTools;
class StringToolsBB {
    /**
    * return a string appending zeros to the begining
    * @param numbers the minimum size of the string (if the original string was bigger no zero will be added)
    **/
    public static function leadingZeros(s:String, numbers:UInt)
    {
        var delta:Int = s.length - numbers;
        while(true){
            if(delta>=0) return s;
            s= "0" +s;
            delta++;
        }
    }
    public static function stripLeadingZeros(s:String)
    {
        while(true){
            if(s.substr(0,1)=="0") s = s.substr(1);
            else break;
        }
        return s;
    }
    public static inline function capFirstLetter(s:String) {
        var t = s.split(" ");
        var r = [];
        for(i in t){
            r.push(i.substr(0,1).toUpperCase() + i.substr(1));
        }
        return r.join(" ");
    }
    public static inline function filenameToTitle(s:String,?after:Bool=false){
        if(after){
            return capFirstLetter(s).replace("_"," ");
        }
        else return capFirstLetter(s.replace("_"," "));
    }
    public static function buildAphabetTree(tab:Array<String>):Map<String ,Array<String> >
        {
        var m:Map<String,Array<String> > = [];
        for(i in tab){
            var alphabet = i.substr(0,1).toUpperCase();
            if(!m.exists(alphabet)) m.set(alphabet, [i]);
            if(m.get(alphabet).indexOf(i) == -1 ) m.get(alphabet).push(i);
        }
        return m;
    }
}
