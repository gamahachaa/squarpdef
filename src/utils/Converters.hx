package utils;

import types.HapaxEnums.Pot;
import types.LowRange;
import types.Regex;

class Converters {
    public static final MAX_VALUE = 16384;
	static public function TO_VALUETYPE(pot:Pot, value:String):String {
		//trace("TO_VALUETYPE",pot,value);
		return switch (pot) {
			case CC: new CCvalue(value);
			case CV: isCV(value);
			case NRPN: isNRPN(value);
			default: "NULL";
		}
	}

	static public function TO_VALUE(pot:Pot, value:String):String {
		return switch (pot) {
			case CC: new CCvalue(value);
			case PB: null;
			case AT: null;
			case CV: isCV(value);
			case NRPN: isNRPN(value);
			case NULL: null;
		};
	}
	static public function STRING_TO_VALUE(index:Int, value:String):String {
		return switch (index) {
			case 1: new CCvalue(value);
			case 2: null;
			case 3: null;
			case 4: isCV(value);
			case 5: isNRPN(value);
			case _: null;
		};
	}

	static public function TO_DEFO_VALUE(pot:Pot, defo:String, ?nrpn7Depth:Bool=true):String {
		if(defo==null || defo=="NULL" || defo=="" ) return null;
		var error = '"$defo"... $pot default must be between 0 and $MAX_VALUE';
        return switch (pot) {
			case CC: new LowRange(defo??"NULL");
			case CV:
				if(defo == null) null
				else if (Regex.cv65535.match(defo) || Regex.voltageValues.match(defo)) defo else
					throw '"$defo" ... For CV: DEFAULT_VALUE must be either a valid number between 0 and 65535, or a voltage between -5V and 5V, e.g. "-4.25V" or "1.7V"';
			case NRPN: if ((nrpn7Depth && IS_BETWEEN(defo, 0 , 128))||(!nrpn7Depth && IS_BETWEEN(defo, 0 , MAX_VALUE))) {
						defo;
					}else if(defo!=null){
                        if (nrpn7Depth) throw '"$defo"... Default RNPN between 0 and 127 (for 7 bit NRPNs)';
                        throw error + ' (for 14 bit NRPNs)';
                    } else null;
            case PB: defo != null ? (IS_BETWEEN(defo, 0 , MAX_VALUE) ? defo: throw error): null;
			default: null;
		};
	}
    static public function IS_BETWEEN(value:String, low:Float,high:Float):Bool{
        final v = Std.parseFloat(value);
		//trace(v >= low , v < high);
       return  v >= low && v < high;
    }
	static inline function isCV(value:String):String {
		return ["1", "2", "3", "4"].indexOf(value) == -1?throw '"$value" ...For CV, value must be between 1 and 4':value;
	}

	static inline function isNRPN(value:String) {
		return
			Regex.nrpnEreg.match(value) ? value : throw '"$value" For NRPN, value must be MSB:LSB:DEPTH, with both lsb & msb bebtween 0 and 127, and DEPTH being either 7 or 14';
	}
}
