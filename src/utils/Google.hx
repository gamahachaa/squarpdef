package utils;
#if js
import haxe.http.HttpJs;
import js.Browser;
#end

using StringTools;

class Google extends HttpJs {
    static final ROOT = "https://google.com";
    public function new() {
        super(ROOT);
    }
    public function search(?brand:String="",?product:String="",?userGuide:Bool=true){
        var q = "?q=";
        var t = [];
        if(brand!="") t.push(brand.urlEncode());
        if(product!="") t.push(product.urlEncode());
        if(userGuide) q += "user+manual+%7C+user+guide+";
        if(t.length >0) {
            q += "allintitle%3A";
            q += t.join("+");
        }

        this.url = ROOT + '/search$q';
        Browser.window.open(this.url,"_blank");
    }
}