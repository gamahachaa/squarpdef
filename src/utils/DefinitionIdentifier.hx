package utils;

import models.*;
import parsers.BaseParser;
import types.Regex;
import utils.DefinitionIdentifier;
/**
 * Identifies which definitionfile it is (Hapax or Pyramid).
 * Regex based
 */
@keep
class DefinitionIdentifier {
	@:keep
	public static function identify(s:String):Class<Definition> {
		var lines:Array<String> = s.split(BaseParser.BR);
        var i = 0;
		while (i<lines.length) {
            var l = lines[i];
			if (Regex.pyraKeyName.match(l)) {
				return PyramidDef;
			} else if (Regex.hapaxVersionLine.match(l)) {
				return HapaxDefinition;
			}
            i++;
		}
		return null;
	}
}
