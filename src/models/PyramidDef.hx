package models;

import haxe.Exception;
import types.*;
import types.Channel;
import types.MidiNote;
import types.Regex;

using Lambda;
using StringTools;
using utils.StringToolsBB;

class PyramidDef extends Definition {
	public inline static var NAME:String = "NAME";
	public inline static var CHANNEL:String = "CHANNEL";
	public inline static var OUT:String = "OUT";
	inline static var CC_MAX:Int = 120;
	inline static var NOTES_MAX:Int = 127;
	public var _warnings:String;
	public function new() {
		super();
		_warnings = "";
	}

	function cleanValue(s:String):String {
		return s.substr(s.indexOf(":") + 1);
	}

	public function getName():String {
		return cleanValue(stringProperties.get(NAME));
	}

	public function getChannel() {
		var c = cleanValue(stringProperties.get(CHANNEL));
		return cleanValue(stringProperties.get(CHANNEL));
	}

	public function getOut() {
		return cleanValue(stringProperties.get(OUT));
	}

	public function getCCs():Map<String, String> {
		return getSubs(Regex.pyraCCkey);
	}

	public function getNotes(?filter:String=""):Map<String, String> {
		return getSubs(Regex.pyraKeyNote,filter);
	}
	public function countNotes():Int{
		return getNotes().array().length;
	}

	public function toTemplate() {
		
		var s = "";
		s = printTag(stringProperties.get(NAME) + "\n");
		s += printTag(stringProperties.get(CHANNEL) + "\n");
		if (stringProperties.exists(OUT)) {
			s += printTag(stringProperties.get(OUT) + "\n");
		}
		s += printAll(getCCs());
		s += printAll(getNotes());
		return s;
	}

	public function checkSum() {
		if (stringProperties == []) {
			throw new Exception('Nothing was apparently parsed ...');
		}
		if (!stringProperties.exists(NAME)) {
			throw new Exception('Pyramid definitions need a NAME...');
		}
		if (!stringProperties.exists(CHANNEL)) {
			throw new Exception('Pyramid definitions need a CHANNEL...');
		}
		if (stringProperties.exists(OUT)) {
			var out = stringProperties.get(OUT);
			if (!Regex.pyraOut.match(out))
				throw new Exception('Pyramid definitions OUT : wrong format "$out", should be A or B or USB');
		}
		// check max CC
		if (getCCs().array().length > CC_MAX) {
			throw new Exception('Pyramid definitions MAX CC = $CC_MAX...');
		}
		// check max notes
		if (getNotes().array().length > NOTES_MAX) {
			throw new Exception('Pyramid definitions MAX notes  = $NOTES_MAX...');
		}
	}

	public function toHapax(?filter:String=""):HapaxDefinition {
		final doFilter = filter!="";
		var h:HapaxDefinition = new HapaxDefinition();
		var drumrow = 1;

		_warnings = "";
		
		for (k => v in this.stringProperties) {
			v = cleanValue(v);
			// trace(k,v);
			switch (k) {
				case PyramidDef.NAME:
					h.trackName = new Name(v);
				case PyramidDef.OUT:
					h.outport = v;
				case PyramidDef.CHANNEL:
					h.outchan = new Channel(v.stripLeadingZeros());
				case _:

					if (Regex.pyraKeyNote.match(k)) {
						//trace(k,v);
						if(drumrow > 8) {
							//trace("drumrow > 8");
							if(doFilter && v.indexOf(filter) >0 )
								_warnings += '$v not added as already 8 drumlanes were added.\n';
							continue;
						}
						else if(doFilter && v.indexOf(filter)  == -1 ){
							//trace("not in filter");
							continue;
						}	
						else{
							//trace("adding drum lane");
							h.addDrumLane(
							new DrumLane(
								drumrow, 
								new LowRange("NULL"), 
								new Channel(this.getChannel().stripLeadingZeros()), 
								new MidiNote(Regex.pyraKeyNote.matched(1)),
								new Name(doFilter ? v.replace("*","").trim() :v,false)
							));
							drumrow++;
						}
						
					} else {
						h.addContrlChange(new ControlChange(new Name(v), new LowRange(k)));
					}
			}

		}
		//trace(_warnings);
		if (h.drumlanne.count() > 0) {
			h.trackType = "DRUM";
		}
		h._version = "1";
		return h;
	}
}
