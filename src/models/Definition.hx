package models;
using StringTools;
class Definition {
    public var stringProperties:Map<String, String>;
    public function new() {
        stringProperties = [];
    }
    // public function printAll(all:Map<Dynamic, Dynamic>,?tag:String):String{
    public function printAll(all:Iterable<Dynamic>,?tag:String,?html:Bool=false):String{
        var s ="";
        var retrn = html?"<br/>":"\n";
        for ( i in all ){
            var blankDefault = ~/(:DEFAULT=\s|:DEFAULT=$)/gi;
            var temp = i.toString().replace(":DEFAULT=NULL","").replace("DEFAULT=NULL","");
            temp = blankDefault.replace(temp," ");
            s += '$temp$retrn';
        }
        //return printTag(s,tag);
        return printTag((html? retrn : "")+s,tag);
    }
    
    public function printTag(s:String,?tag:String,?html:Bool=false){
        var retrn = html?"<br/>":"\n";
        return tag == null ? '$s$retrn' : '[$tag]$retrn$s[/$tag]';
    }
    function getSubs(reg:EReg,?filter=""):Map<String,String>{
        var m:Map<String,String> = [];
        for(k=>v in stringProperties){
            if(reg.match(k)){
                if((filter !="" && v.indexOf(filter)> -1) || filter =="") m.set(k,v);
            }
        }
        return m;
    }
    
}
