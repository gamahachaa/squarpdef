package models;

import UInt;
import types.*;
import types.DrumLane;

using Lambda;
using StringTools;
using utils.StringToolsBB;

/**
 * @todo make reset 
 */
class HapaxDefinition extends Definition {
	public inline static var VERSION = "VERSION";
	public inline static var TRACKNAME = "TRACKNAME";
	public inline static var TYPE = "TYPE";
	public inline static var OUTPORT = "OUTPORT";
	public inline static var OUTCHAN = "OUTCHAN";
	public inline static var INPORT = "INPORT";
	public inline static var INCHAN = "INCHAN";
	public inline static var MAXRATE = "MAXRATE";
	public inline static var COMMENT = "COMMENT";
	public static var DRUMLANES = DrumLane.TAG;
	public static var CONTROL_CHANGES = ControlChange.TAG;
	public static var CONTROL_CHANGES_PAIRS = ControlChangePair.TAG;
	public static var PROGRAM_CHANGES = ProgramChange.TAG;
	public static var NRPN = Nrpn.TAG;
	public static var POT_ASSIGN = PotAssign.TAG;
	public static var AUTOMATION = Automation.TAG;
	public static var TEMPLATE_COMMENT = haxe.Resource.getString("writable_template");
	public static var TEMPLATE_NOCOMMENT = haxe.Resource.getString("writable_template_nocomment");
	public static var TEMPLATE_COMMENT_HTML = haxe.Resource.getString("writable_template_html");
	public static var TEMPLATE_NOCOMMENT_HTML = haxe.Resource.getString("writable_template_html_nocomment");
	public static var TABLE_SOLO_HTML = haxe.Resource.getString("table_solo_template_html");
	@:isVar public var trackName(get, set):Name;
	@:isVar public var trackType(get, set):String;
	@:isVar public var outport(get, set):String;
	@:isVar public var inport(get, set):String;

	@:isVar public var outchan(get, set):Channel;
	@:isVar public var inchan(get, set):Channel;
	@:isVar public var maxrate(get, set):Rate;
	@:isVar public var comment(get, set):String;
	@:isVar public var _version(get, set):String;

	@:isVar public var drumlanne(get, set):Map<Int, DrumLane>;
	@:isVar public var cc(get, set):Map<LowRange, ControlChange>;
	@:isVar public var ccp(get, set):Map<String, ControlChangePair>;
	@:isVar public var pc(get, set):Map<Name, ProgramChange>;
	@:isVar public var nrpn(get, set):Map<String, Nrpn>;
	@:isVar public var potAssign(get, set):Map<PotNumber, PotAssign>;
	@:isVar public var automation(get, set):Array<Automation>;
	public var _warnings:String;
	public var _errors:String;

	/**
	 * Constructor
	**/
	public function new() {
		super();
		_warnings = "";
		_errors = "";
		trackName = new Name("NULL");
		_version = "1";
		trackType = "NULL";
		outport = "NULL";
		inport = "NULL";
		outchan = new Channel("NULL");
		inchan = new Channel("NULL");
		maxrate = new Rate("NULL");
		drumlanne = [];
		cc = [];
		ccp = [];
		pc = [];
		nrpn = [];
		potAssign = [];
		automation = [];
		comment = "";
	}

	/**
		@todo test with null values
	**/
	public function toTemplate(?withComments:Bool = true, ?toHtml:Bool = false) {
		// trace(withComments);
		var t = if(withComments){
			toHtml ? TEMPLATE_COMMENT_HTML : TEMPLATE_COMMENT;
		}else{
			toHtml ? TEMPLATE_NOCOMMENT_HTML : TEMPLATE_NOCOMMENT;
		}
		trace(t);
		trace(this);
		if (drumlanne.array().length > 0)
			this.stringProperties.set(DRUMLANES, toHtml ? printTableCells(drumlanne, DRUMLANES) : printAll(drumlanne, DRUMLANES));
		else this.stringProperties.set(DRUMLANES,"");
		if (cc.array().length > 0)
			this.stringProperties.set(CONTROL_CHANGES, toHtml ? printTableCells(cc, CONTROL_CHANGES) : printAll(cc, CONTROL_CHANGES));
		else this.stringProperties.set(CONTROL_CHANGES,"");
		if (ccp.array().length > 0)
			this.stringProperties.set(CONTROL_CHANGES_PAIRS, toHtml ? printTableCells(ccp, CONTROL_CHANGES_PAIRS) : printAll(ccp, CONTROL_CHANGES_PAIRS));
		else this.stringProperties.set(CONTROL_CHANGES_PAIRS,"");
		if (pc.array().length > 0)
			this.stringProperties.set(PROGRAM_CHANGES, toHtml ? printTableCells(pc, PROGRAM_CHANGES) : printAll(pc, PROGRAM_CHANGES));
		else this.stringProperties.set(PROGRAM_CHANGES,"");
		if (nrpn.array().length > 0)
			this.stringProperties.set(NRPN, toHtml ? printTableCells(nrpn, NRPN) : printAll(nrpn, NRPN));
		else this.stringProperties.set(NRPN,"");
		if (potAssign.array().length > 0)
			this.stringProperties.set(POT_ASSIGN, toHtml ? printTableCells(potAssign, POT_ASSIGN) : printAll(potAssign, POT_ASSIGN));
		else this.stringProperties.set(POT_ASSIGN,"");
		if (automation.array().length > 0)
			this.stringProperties.set(AUTOMATION, toHtml ? printTableCells(automation, AUTOMATION) : printAll(automation, AUTOMATION));
		else this.stringProperties.set(AUTOMATION,"");
		this.stringProperties.set(VERSION, _version);
		this.stringProperties.set(INPORT, inport);
		this.stringProperties.set(OUTCHAN, outchan);
		this.stringProperties.set(INCHAN, inchan);
		this.stringProperties.set(MAXRATE, maxrate);
		// this.stringProperties.set(COMMENT, printTag(comment, toHtml ? null : COMMENT, toHtml));
		this.stringProperties.set(COMMENT, printAll([comment], COMMENT));
		this.stringProperties.set(OUTPORT, outport);
		this.stringProperties.set(TRACKNAME, trackName);
		this.stringProperties.set(TYPE, trackType);

		for (k => v in stringProperties) {
			t = t.replace('<$k>', '$v');
		}

		return t;
	}
	public function printTableCells(all:Iterable<Dynamic>, title:String, ?columns:Int=3):String
		{
			var template = TABLE_SOLO_HTML;
			var size = all.array().length;
			var limit = Math.max(size /columns, 3);
			template = template.replace("<TITLE>",title);
			var s ="";
			var rO = "<td>";
			var rC = "</td>";
			var l = limit;
			for (i in all){
				if(l == limit) s += rO;
				s += '$i</br>';
				if(l == 1){
					s += rC;
					l = limit;
				} else l--;
			}
			if(s.substr(-5)!=rC) s +=rC;
			template = template.replace("<TAG>",s);
			return template; 
		}
	public function toPyramid(?channel:String, ?out:String):PyramidDef {
		_warnings = "";
		var sep = ":";
		var pyra:PyramidDef = new PyramidDef();
		pyra.stringProperties.set(PyramidDef.NAME, PyramidDef.NAME + sep + this.trackName);
		var _out = out == null ? switch (this.outport) {
			case "A" | "B": this.outport;
			case "USBD" | "USBH": "USB";
			case _: "A";
		} : out;
		pyra.stringProperties.set(PyramidDef.OUT, PyramidDef.OUT + sep + _out);
		var chan = channel == null ? switch (cast(this.outchan, String)) {
			case "ALL" | "NULL": "1";
			case _: this.outchan;
		} : channel;
		pyra.stringProperties.set(PyramidDef.CHANNEL, PyramidDef.CHANNEL + sep + chan);
		if (hasDrumLanes()) {
			for (i in drumlanne) {
				final k = "N" + i.note.leadingZeros(2);
				if (pyra.stringProperties.exists(k)) {
					this._warnings += k + " duplicate note number (" + pyra.stringProperties.get(k) + ") for " + i.name;
				} else {
					pyra.stringProperties.set(k, "N" + i.note.leadingZeros(2) + sep + i.name);
				}
			}
		}
		// trace(cc);
		for (i in cc) {
			if (pyra.stringProperties.exists(i.cc)) {
				this._warnings += i.cc + " duplicate cc for " + i.name;
			}
			pyra.stringProperties.set(i.cc, i.cc + sep + i.name);
		}
		return pyra;
	}

	function hasDrumLanes() {
		return drumlanne.array().length > 0;
	}

	public function addAutomation(a:Automation) {
		if (automation.length < Automation.MAX) {
			automation.push(a);
		} else
			throw '${automation.length} already... NOTE: You are limited to 64 automation lanes}';
	}

	public function addPot(pot:PotAssign) {
		this.potAssign.set(pot.pot, pot);
	}

	public function addNrpn(n:Nrpn) {
		this.nrpn.set(n.id, n);
	}

	public function addDrumLane(d:DrumLane) {
		this.drumlanne.set(d.row, d);
	}

	public function addProgramChange(p:ProgramChange) {
		this.pc.set(p.name, p);
	}

	public function addContrlChange(c:ControlChange) {
		this.cc.set(c.cc, c);
	}

	public function addContrlChangePair(cp:ControlChangePair) {
		if(ccp.exists(cp.id)) throw 'Duplicate CC pair \nprevious: ${ccp.get(cp.id)}';
		this.ccp.set(cp.id, cp);
	}

	/******************************* GET-SET ******************/
	function get_drumlanne():Map<UInt, DrumLane> {
		return drumlanne;
	}

	function set_drumlanne(value:Map<UInt, DrumLane>):Map<UInt, DrumLane> {
		return this.drumlanne = value;
	}

	function get_cc():Map<LowRange, ControlChange> {
		return cc;
	}

	function set_cc(value:Map<LowRange, ControlChange>):Map<LowRange, ControlChange> {
		return this.cc = value;
	}

	function get_ccp():Map<String, ControlChangePair> {
		return ccp;
	}

	function set_ccp(value:Map<String, ControlChangePair>):Map<String, ControlChangePair> {
		return this.ccp = value;
	}

	function get_pc():Map<Name, ProgramChange> {
		return pc;
	}

	function set_pc(value:Map<Name, ProgramChange>):Map<Name, ProgramChange> {
		return this.pc = value;
	}

	function get_nrpn():Map<String, Nrpn> {
		return nrpn;
	}

	function set_nrpn(value:Map<String, Nrpn>):Map<String, Nrpn> {
		return this.nrpn = value;
	}

	function get_potAssign():Map<PotNumber, PotAssign> {
		return potAssign;
	}

	function set_potAssign(value:Map<PotNumber, PotAssign>):Map<PotNumber, PotAssign> {
		return this.potAssign = value;
	}

	function get_automation():Array<Automation> {
		return automation;
	}

	function set_automation(value:Array<Automation>):Array<Automation> {
		return this.automation = value;
	}

	function get__version():String {
		return _version;
	}

	function set__version(value:String):String {
		return this._version = value;
	}

	function get_inport():String {
		return inport;
	}

	function set_inport(value:String):String {
		return this.inport = value;
	}

	function get_outchan():Channel {
		return outchan;
	}

	function set_outchan(value:Channel):Channel {
		return this.outchan = value;
	}

	function get_inchan():Channel {
		return inchan;
	}

	function set_inchan(value:Channel):Channel {
		return this.inchan = value;
	}

	function get_maxrate():Rate {
		return maxrate;
	}

	function set_maxrate(value:Rate):Rate {
		return this.maxrate = value;
	}

	function get_comment():String {
		return comment;
	}

	function set_comment(value:String):String {
		return this.comment = value;
	}

	function get_trackName():Name {
		return trackName;
	}

	function get_outport():String {
		return outport;
	}

	function set_outport(value:String):String {
		return this.outport = value;
	}

	function set_trackName(value:Name):Name {
		return this.trackName = value;
	}

	function get_trackType():String {
		return trackType;
	}

	function set_trackType(value:String):String {
		return this.trackType = value;
	}
}
