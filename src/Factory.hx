
import controllers.*;
import haxe.Exception;
import haxe.ui.HaxeUIApp;
import types.HapaxEnums;
import views.Dialoging;

class Factory {
    final _app:HaxeUIApp;
    
    public function new(app:HaxeUIApp) {
        _app = app;

        HapaxEnums.POPULATE_ALL();
    }
    /*public function createConverterApp() {
       
        try{
            final converterControler:ConverterControler = new  ConverterControler();
            _app.addComponent(converterControler.view);
            
        }
        catch(e:Exception){
            Dialoging.showError("Error", e.message);
        }
        
    }*/
    public function createHapaxEditor(app){
        try{
            final hapaxControler:HapaxEditorController = new  HapaxEditorController();
            _app.addComponent(hapaxControler.view);
            
        }
        catch(e:Exception){
            Dialoging.showError("Error", e.message + "\n" +e.stack);
        }
    }
}