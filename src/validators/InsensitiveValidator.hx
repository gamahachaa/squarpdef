package validators;

import haxe.ui.validators.PatternValidator;

class InsensitiveValidator extends PatternValidator {
	static public var TEST:String = "hello|world";

	public override function setProperty(name:String, value:Any) {
		trace(value);
		switch (name) {
			case "pattern":
				pattern = new EReg(Std.string(value), "gmi");
			case _:
				super.setProperty(name, value);
		}
	}
}
