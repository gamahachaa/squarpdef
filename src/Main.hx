package;

import haxe.crypto.Md5;
import haxe.ui.HaxeUIApp;
import haxe.ui.Toolkit;
import haxe.ui.themes.Theme;
import haxe.ui.tooltips.ToolTipManager;

using StringTools;






class Main {
	public static function main() {
        
		Toolkit.theme = Theme.DARK;
		//trace(Md5.encode(Date.now().toString()), Date.now().toString());
		var app = new HaxeUIApp();
		app.ready(function() {
			var factory = new Factory(app);
			ToolTipManager.followMouse = true;
			factory.createHapaxEditor(app);
			app.start();
		});
	}
}
