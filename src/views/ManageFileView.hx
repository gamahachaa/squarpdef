package views;
import haxe.Exception;
import haxe.ui.components.TextArea;
import haxe.ui.containers.VBox;
import haxe.ui.containers.dialogs.Dialog.DialogButton;
import haxe.ui.containers.dialogs.Dialogs;
import haxe.ui.containers.dialogs.MessageBox.MessageBoxType;
import haxe.ui.containers.dialogs.OpenFileDialog;
import haxe.ui.containers.dialogs.SaveFileDialog;
import signals.Signal.Signal0;
import signals.Signal1;
import signals.Signal2;
#if js
import js.Browser;
#end
/**
 * Parent class for saving/openeing file.
 * It used 
 */
class ManageFileView extends VBox{
    public var fileLoaded:Signal1<SelectedFileInfo>;
    
    public var saveSignal:Signal2<String, Bool>;
	// public var pasted:Signal1<String>;
	public var printSignal:Signal0;
	var pasteContainer:TextArea;
    public function new() {
        super();
        fileLoaded = new Signal1<SelectedFileInfo>();
        saveSignal = new Signal2<String,Bool>();
		// pasted = new Signal1<String>();
		printSignal = new Signal0();
		
    }
	
    public function doSaveFile(?fileName:String, ?content:String) {
		//trace("doSaveFile");
		var dialog = new SaveFileDialog();
		dialog.options = {
			title: "Save definition File",
			writeAsBinary: false,
			extensions: FileDialogTypes.TEXTS
		}
		dialog.onDialogClosed = function(event) {
			if (event.button == DialogButton.OK) {
				Dialogs.messageBox('File\n${fileName}\nsaved (in your download fodler)!', "Save Result", MessageBoxType.TYPE_INFO);
			}
		}
		dialog.fileInfo = {
			name: fileName,
			text: content
		}
		dialog.show();
	}
    private function onOpenFile(_) {
		var dialog = new OpenFileDialog();
		dialog.options = {
			readContents: true,
			title: "Open definition File",
			readAsBinary: true,
			extensions: FileDialogTypes.TEXTS
		};
		dialog.onDialogClosed = function(event) {
			if (event.button == DialogButton.OK) {
				fileLoaded.dispatch(dialog.selectedFiles[0]);
			}
		}
		dialog.show();
	}
}