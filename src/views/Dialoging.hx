package views;
import haxe.ui.containers.dialogs.MessageBox;

class Dialoging {
    public static function showError(title:String, message:String) {
		showDialog(title,message,MessageBoxType.TYPE_ERROR);
	}
	public static function showWarning(title:String, message:String) {
		showDialog(title,message,MessageBoxType.TYPE_WARNING);
	}
	public static function showInfo(title:String, message:String) {
		showDialog(title,message,MessageBoxType.TYPE_INFO);
	}
	static function showDialog(title:String, message:String, type:MessageBoxType) {
		var d:MessageBox = new MessageBox();
		d.type = type;
		d.width = 800;
		d.height = 600;
		d.title = title;
		d.message = message;
		d.show();
	}
}