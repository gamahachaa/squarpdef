package views.components;

import haxe.ui.containers.menus.Menu;
import haxe.ui.containers.menus.MenuItem;
import haxe.ui.events.ItemEvent;
import haxe.ui.events.MenuEvent;
import signals.Signal3;
import types.HapaxEnums;
import views.windows.BaseInteractiveTables;
import views.windows.IControlSender;

class Controls<T> extends BaseInteractiveTables<T> implements IControlSender{
	var _default:T;
	public var sharingControlData:Signal3<ItemEvent, String, Pot>;
	public function new(defaultNull:Dynamic) {
		super();
		sharingControlData = new Signal3<ItemEvent, String, Pot>();
		_default = defaultNull;
	}
	// public function getDefaultNull(){
	// 	return _default;
	// }
	function isDefault(obj:Dynamic) {
		var defileds = Reflect.fields(_default);
		for(i in defileds){
			if(!Reflect.hasField(obj, i )) return false;
			if(Reflect.field(obj,i) != Reflect.field(_default, i)) return false;
		}
		return true;
	}
	public function addDefault() {
		addItemInTable(Reflect.copy(_default));
	}
	public function deleteItem(e:ItemEvent):Void{
		onDeleteTableItem(e);
	}
	override function onDeleteTableItem(e:ItemEvent) {
		e.bubble = false;
		super.onDeleteTableItem(e);
	}

	override public function populate(arg:Array<T>) {
		// var ccs:Map<LowRange, ControlChange>;
		try {
			if (arg != null) {
				for (i in arg) {
					tableView.dataSource.add(cast(i));
				}
			}
			super.populate(null);
		} catch (e) {
			throw 'Looks like the wrong type was provided for Control ${e.message}';
		}
	}
}
@:xml('
<menu>
    <menu-item text="Remove!" id="delete"/>
	<menu-item text="Send to Automation" id="toAuto"/>
    <menu text="Send to Pot" id="potsToSend">
		<menu-item text="1" id="Pot1"/>
		<menu-item text="2" id="Pot2"/>
		<menu-item text="3" id="Pot3"/>
		<menu-item text="4" id="Pot4"/>
		<menu-item text="5" id="Pot5"/>
		<menu-item text="6" id="Pot6"/>
		<menu-item text="7" id="Pot7"/>
		<menu-item text="8" id="Pot8"/>
	</menu>
</menu>
')
class MyMenu extends Menu {
	var sender:IControlSender;
	var senderEvent:ItemEvent;
	var pot:types.HapaxEnums.Pot;
	public function new(sender:IControlSender, e:ItemEvent, type:types.HapaxEnums.Pot) {
		super();
		pot = type;
		this.sender = sender;
		senderEvent = e;
	
		this.onMenuSelected = onMenuClicked;
	
	}
	// @:bind(this,MouseEvent.CLICK)
	function onMenuClicked(e:MenuEvent){
		switch (e.menuItem.id){
			case "delete" : sender.deleteItem(senderEvent);
			case _ : sender.sharingControlData.dispatch(senderEvent, e.menuItem.id, pot);
		}
	}
}