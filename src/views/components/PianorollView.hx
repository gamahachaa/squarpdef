package views.components;

import haxe.ui.Toolkit;
import haxe.ui.components.Label;
import haxe.ui.containers.Box;
import haxe.ui.containers.HBox;
import haxe.ui.containers.SideBar;
import haxe.ui.core.ItemRenderer;
import haxe.ui.events.MouseEvent;
import haxe.ui.styles.StyleSheet;
import haxe.ui.tooltips.ToolTipManager;
import signals.Signal2;
import types.MidiNote;

using Lambda;
using StringTools;

// typedef Pitch = {
// 	var musical:String;
// 	var numerical:UInt;
// The class hierarchy of "views.components.PianorollView" does not contain the root node of "assets/views/components/pianoroll.xml" (haxe.ui.containers.HBox) - this may have unintended consequences
// }

@:build(haxe.ui.ComponentBuilder.build("assets/views/components/pianoroll.xml"))
class PianorollView extends SideBar {
	// static final notes:Array<String> = ["c", "c#", "d", "d#", "e", "f", "f#", "g", "g#", "a", "a#", "b"];
	static public final NULLNoteObj:Pitch = {musical: null, numerical: null};

	public var selectedNoteObject:Pitch;
	public var hoveredNoteObject:Pitch;

	var all:Array<Box>;
	var whitesKeys:Array<Box>;
	var blacksKeys:Array<Box>;
	var spacersKeys:Array<Box>;
	var octaves:Array<Box>;
	var numberingLabels:Array<HBox>;

	public var noteSignal:Signal2<UInt, Pitch>;

	var tmpIndex:UInt;

	public function new() {
		super();

		// ToolTipManager.defaultRenderer = new CustomToolTip();
		tmpIndex = -1;
		this.clearNoteBtn.onClick = (e) -> {
			trace(e.target.id);
			this.noteSignal.dispatch(tmpIndex, null);
		}
		noteSignal = new Signal2<UInt, Pitch>();
		selectedNoteObject = NULLNoteObj;
		this.method = "float";
		this.position = "top";
		this.modal = true;
		this.left = 0;
		this.onClick = (e) -> {
			this.selectedNoteObject = this.hoveredNoteObject = NULLNoteObj;
			cleanAll();
			this.hide();
			tmpIndex = -1;
			this.noteSignal.remove(true);
		};

		whitesKeys = this.findComponents("white", Box);
		blacksKeys = this.findComponents("black", Box);
		spacersKeys = this.findComponents("space", Box);
		octaves = this.findComponents("piano", Box);
		all = whitesKeys.concat(blacksKeys).concat(spacersKeys);

		updateDisplay();
		for (i in all) {
			// trace(i.id, i.parentComponent.parentComponent.id);
			i.onClick = onNoteClick;
			i.onMouseOver = onhover;
			i.onMouseOut = onout;
		}
	}

	function close() {
		ToolTipManager.defaultDelay = 500;
		ToolTipManager.fade = true;
		ToolTipManager.followMouse = true;
		cleanAll();
		this.hide();
	}

	function open() {
		ToolTipManager.defaultDelay = 10;
		ToolTipManager.fade = false;
		ToolTipManager.followMouse = true;
		this.show();
	}

	function onNoteClick(e:MouseEvent) {
		// cleanAll();
		// selectedNoteObject = getMidiNoteFromBoxes(e.target.id, e.target.parentComponent.parentComponent.id);
		noteSignal.dispatch(tmpIndex, getMidiNoteFromBoxes(e.target.id, e.target.parentComponent.parentComponent.id));
		this.selectedNoteObject = this.hoveredNoteObject = NULLNoteObj;
		//
		close();
	}

	function onhover(e:MouseEvent) {
		// trace(e.target.id, e.target.parentComponent.parentComponent.id);
		hoveredNoteObject = getMidiNoteFromBoxes(e.target.id, e.target.parentComponent.parentComponent.id);
		final key = findSelectedNoteFromNum(hoveredNoteObject.numerical);
		// updatePrompt();
		key.addClass("selected");
	}

	function onout(e:MouseEvent) {
		hoveredNoteObject = selectedNoteObject ?? NULLNoteObj;
		// e.target.removeClass("selected");
		cleanAll();
		// updatePrompt();
		showSelectedNote();
	}

	function updatePrompt(data:Dynamic) {
		// trace("updatePrompt", '${data.row}\n${data.name}\n${data.chan}');
		var chan:String = Std.isOfType(data.chan, String) ? data.chan : data.chan.text;
		chan = chan.toUpperCase() == "NULL" ? "" : chan;
		this.rollPrompt.text = 'Row ${data.row}\n${data.name}\n$chan';
	}

	public function onNoteRequest(i:UInt, data:Dynamic) {
		cleanAll();
		tmpIndex = -1;

		updatePrompt(data);
		if (this.hidden) {
			// trace(i, data);
			tmpIndex = i;
			// trace(MidiNote.FromNumerical(data));
			if (data.notenumber == "")
				this.selectedNoteObject = PianorollView.NULLNoteObj;
			else
				this.selectedNoteObject = MidiNote.FromNumerical(MidiNote.StringToPitch(data.notenumber).numerical);
			this.showSelectedNote();
			// this.show();
			open();
		}
	}

	function updateDisplay() {
		var mainW = Toolkit.screen.actualWidth;
		var W = mainW < 1236 ? mainW < 861 ? mainW < 698 ? 43 : 57 : 71 : 106;
		var WH = Math.ceil(W / 7);
		var WHh = WH * 4;
		var SP = WH / 3;
		var BK = SP * 2;
		this.styleSheet = new StyleSheet();
		this.styleSheet.parse('
			#numbering{padding-top:${WHh - 12}px;}
			.piano{spacing:0;}#numbering{font-size:${Math.ceil(BK + 1)};}
			#rollPrompt{padding:${W / 8}px ${W / 4}px;font-style:bold;font-size:${W / 8}px;}
			');
		var s = 0;
		var sumOctaves = 0.0;
		for (i in octaves) {
			i.width = W;
			sumOctaves += Math.ceil(i.actualComponentWidth);
			i.id = "oct" + s;
			i.findComponent("numbering", Label).text = Std.string(s++);
		}
		for (i in whitesKeys) {
			i.width = WH;
			i.height = WHh;
			// i.tooltip = i.id + i.parentComponent.parentComponent.id.replace("oct","");
		}
		for (i in blacksKeys) {
			i.width = BK;
			i.height = WHh * 3 / 5;
		}
		for (i in spacersKeys) {
			i.width = SP;
			i.height = WHh * 3 / 5;
			/*i.height = 2;*/
		}

		for (i in all) {
			i.tooltip = MidiNote.PitchToString(getMidiNoteFromBoxes(i.id, i.parentComponent.parentComponent.id));
		}

		this.height = WHh + 12;
		this.width = sumOctaves + 70;
		this.percentWidth = 100;
		pianobox.horizontalAlign = "center";
		// pianobox.left = (mainW - (sumOctaves +70))/2;
		// trace(this.width);
	}

	public function showSelectedNote() {
		// trace(b != null && selectedNoteObject != NULLNoteObj );
		if (selectedNoteObject != NULLNoteObj) {
			final b = findSelectedNoteFromNum(selectedNoteObject.numerical);
			b.addClass("selected");
			this.clearNoteBtn.hidden = false;
		}
		// updatePrompt();
	}

	function getMidiNoteFromBoxes(note:String, octave:String):Pitch {
		final _note = StringTools.replace(note, "s", "#");
		final _oct = StringTools.replace(octave, "oct", "");
		return {
			musical: (_note + _oct).toUpperCase(),
			numerical: MidiNote.GETNumericalNoteFromMusicalPair(_note, _oct)
		}
	}

	function findSelectedNoteFromNum(num:String):Box {
		final noteAndOctave = MidiNote.GETNoteAndOctaveFromNum(num);
		final octBox = findComponent("oct" + noteAndOctave.octave, Box);
		return octBox.findComponent(StringTools.replace(noteAndOctave.note, "#", "s"), Box);
	}

	function cleanAll() {
		this.clearNoteBtn.hidden = true;
		for (i in all) {
			// i.removeClass("selected");
			if (i.hasClass("selected")) {
				i.removeClass("selected");
			}
		}
	}
}

@:xml('
<item-renderer layout="horizontal" width="200">
<style>
	#content {background : #de7504 #ff8605 horizontal;color:#ffffff;}
</style>
	<hbox id="container"  width="100%" >
		<label id="content" width="100%" />
	</hbox>
</item-renderer>
')
private class CustomToolTip extends ItemRenderer {}
