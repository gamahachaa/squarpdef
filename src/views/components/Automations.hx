package views.components;

import haxe.ui.events.ItemEvent;
import signals.Signal3;

class Automations<T> extends Controls<T> {
	public var canIAdd:Signal3<Automations<T>, Bool, Dynamic>;
	public var data:Array<T>;
	public function new(defaultObject:Dynamic) {
		super(defaultObject);
		data = [];
		canIAdd = new Signal3<Automations<T>, Bool, Dynamic>();
	}

	function onAddItem(e:haxe.ui.events.MouseEvent) {
		if(e != null) e.bubble = false;
		canIAdd.dispatch(this, true, null);
		// super.onAddItem(e);
	}

	override function onDeleteTableItem(e:ItemEvent) {
		super.onDeleteTableItem(e);
		if (e.source.id == "add")
			canIAdd.dispatch(this, false, null); // update count
	}

	override public function populate(arg:Array<T>) {
		super.populate(null);
		canIAdd.dispatch(this, false, null); // trigereg to compute total automations
	}
}
