package views.components;
#if js
import js.Browser;
#end
import haxe.Exception;
import haxe.ui.containers.dialogs.Dialog.DialogButton;
import haxe.ui.containers.dialogs.Dialog;
import haxe.ui.notifications.NotificationManager;
import haxe.ui.notifications.NotificationType;
import signals.Signal1;

using haxe.ui.animation.AnimationTools;


@:build(haxe.ui.macros.ComponentMacros.build("assets/views/components/paster.xml"))
class Paster extends Dialog {
    var pasted:Signal1<String>;
    public function new(signal:Signal1<String>) {
        super();
        this.pasted = signal;
        buttons = DialogButton.CANCEL | DialogButton.OK ;
       // defaultButton = DialogButton.OK;
		this.title = "Definition text converter";
        pasted = new Signal1<String>();
        #if js
		// Browser.document.addEventListener("paste", onPaste);
		#end
    }
    function onPaste(_) {
		if(content == null){
			throw new Exception("Paste container is undefined");
		}
		pasted.dispatch(content.text);
	}
    public override function validateDialog(button:DialogButton, fn:Bool->Void) {
        var valid = true;
		var errorText = "Content should not be blank";
        if (button == DialogButton.OK) {
            if (content.text == "" || content.text == null ) {
                content.flash();
                valid = false;
            }else{

            }
            if (valid == false) {
                NotificationManager.instance.addNotification({
                    title: "Problem with the file name!",
                    body: errorText,
                    type: NotificationType.Error
                });
                this.shake();
            } else {
                NotificationManager.instance.addNotification({
                    title: "Good name choice !",
                    body: "Importing the definition !",
                    type: NotificationType.Success
                });
            }
        }
        fn(valid);
    }
}
