package views.components;

import haxe.Exception;
import haxe.ui.events.ItemEvent;
import signals.Signal2;
import types.Automation;
import types.Control;
import types.HapaxEnums.Pot;
import utils.Converters;
import views.windows.ITransferable;
import views.windows.WindowAutomations.AutoErrors;

@:build(haxe.ui.ComponentBuilder.build("assets/views/components/nrpn_automation.xml"))
class NRPNAutomations extends Automations<Automation> implements ITransferable {
	public var needControlData:Signal2<ITransferable, types.HapaxEnums.Pot>;

	public function new() {
		super({
			msb: "",
			lsb: "",
			depth: false
		});
		needControlData = new Signal2<ITransferable, Pot>();
		getComps();
		tableView.findComponent("add").onClick = onclickAdd;
	}

	function onclickAdd(_) {
		needControlData.dispatch(this, NRPN);
	}

	@:bind(this, ItemEvent.COMPONENT_CLICK_EVENT)
	override function onDeleteTableItem(e:ItemEvent) {
		super.onDeleteTableItem(e);
	}

	override public function populate(arg:Array<Automation>) {
		// reset();
		for (i in arg) {
			var tmp = i._value.split(":");
			var d = tmp[2] == "14";
			this.tableView.dataSource.add({
				msb: tmp[0],
				lsb: tmp[1],
				depth: d,
				defo: i.defaultValue
			});
		}
		super.populate(null);
	}

	override public function validate(?force:Bool = false):Bool {
		if (super.validate() && !force)
			return true;
		else {
			var automations:Array<Automation> = [];
			var errors:Array<AutoErrors> = [];
			for (i in 0...tableView.dataSource.size) {
				var a = tableView.dataSource.get(i);
				var depthIs14 = a.depth;
				var val = '${a.msb}:${a.lsb}:${a.depth ? "14" : "7"}';
				try {
					if (!depthIs14
						&& !(a.msb == "" || a.msb == "0" || a.msb.toUpperCase() == "NULL")
						&& Converters.IS_BETWEEN(a.lsb, 128, Converters.MAX_VALUE)) {
						throw 'NRPN LSB row ${i + 1} DEPTH is 7 and MSB is "${a.msb}" but LSB is "${a.lsb}".\nLSB can be over 127 only if DEPTH is 14 and MSB is 0 or NULL';
					}
					automations.push(new Automation(NRPN, val, "NULL"));
				} catch (e:haxe.Exception) {
					errors.push({
						row: i + 1,
						message: e.message,
						value: a.val,
						typ: NRPN
					});
				}
			}
			data = automations;
			// trace(data);
			if (errors.length > 0) {
				for (i in errors) {
					dispatchErrorNotif('NRPN AUTOMATION on row ${i.row}',
						'NRPN, value must be MSB:LSB:DEPTH, \nwith both lsb & msb bebtween 0 and 127, and DEPTH being either 7 or 14\n\nCurrent value:"${i.value}"');
				}
				return false;
			} else if (tableView.dataSource.size > 0 && hasChanges) {
				dispatchNoErorNotification();
			}
			hasChanges = false;
			return true;
		}
	}

	public function setFromControl(control:Control, pot:types.HapaxEnums.Pot) {
		trace(control, pot);
		try {
			if (control.name == "BLANK") {
				onAddItem(null);
			} else {
				var tmp = control.id.split(":");
				canIAdd.dispatch(this, true, {
					msb: tmp[0],
					lsb: tmp[1],
					depth: tmp[2] == "14"
				});
			}
			// var ass:PotAssign = new PotAssign(new PotNumber(this.selectedIndex), pot, control.id, control.defaultValue);
			// setPot(ass);
		} catch (e:Exception) {
			trace(e.message);
		}
	}
}
