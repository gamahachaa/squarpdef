package views.components;

import haxe.Exception;
import haxe.ui.events.ItemEvent;
import haxe.ui.events.MouseEvent;
import types.ControlChangePair;
import types.LowRange;
import types.Name;

@:build(haxe.ui.ComponentBuilder.build("assets/views/components/ccp_control.xml"))
class CCPControl extends Controls<ControlChangePair> {
    public var data: Map<String, ControlChangePair>;
    public function new() {
        super({
            name:"",
            cc:"",
            defaultValue:""
        });
        getComps();
		data = new Map<String, ControlChangePair>();
    }
    @:bind(add, MouseEvent.CLICK)
	function onAddItemInTable(e:MouseEvent) {
		addDefault();
	}
    @:bind(tableView, ItemEvent.COMPONENT_CLICK_EVENT)
	override function onDeleteTableItem(e:ItemEvent) {
		super.onDeleteTableItem(e);
	}

	override public function validate(?force:Bool=false):Bool {
		if (super.validate() && !force)
			return true;
		else {
			data = [];
			var noErrors = true;
			var cc:Dynamic;
			if (tableView.dataSource != null) {
				for (i in 0...tableView.dataSource.size) {
					cc = tableView.dataSource.get(i);
					try {
						if(cc.name.trim() == "" || cc.name == null) throw "Why create a Control Change withtout a name ?!? ";
						var name:Name = new Name(cc.name, false);
						// ccpDS.add(cc);
						data.set('${cc.msb}:${cc.lsb}',
							new ControlChangePair(name, new LowRange(cc.msb ?? "NULL"), new LowRange(cc.lsb ?? "NULL"), cc.defaultValue));
					} catch (e:Exception) {
						// trace(cc, e);
						noErrors = false;
						dispatchErrorNotif('name on row ${i + 1}', '${e.message}');
					}
				}
				// trace(data, noErrors);
				if (!noErrors)
					return false;
				if (hasChanges) {
					this.dispatchNoErorNotification();
				}
			}
			hasChanges = false;
			return true;
		}
	}
}