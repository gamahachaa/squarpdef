package views.components;

import haxe.Exception;
import haxe.ui.events.ItemEvent;
import signals.Signal2;
import types.Automation;
import types.Control;
import types.HapaxEnums.Pot;
import views.windows.ITransferable;
import views.windows.WindowAutomations.AutoErrors;

// typedef CcAuto = {
// 	var val:String;
// 	var defaultValue:String;
// }

@:build(haxe.ui.ComponentBuilder.build("assets/views/components/cc_automation.xml"))
class CCAutomations extends Automations<Automation> implements ITransferable{
	public var needControlData:Signal2<ITransferable, types.HapaxEnums.Pot>;
	public function new() {
		super({
			val: "",
			defaultValue: ""
		});
		needControlData = new Signal2<ITransferable, Pot>();
		getComps();
		// tableView.findComponent("add").onClick = onAddItem;
		tableView.findComponent("add").onClick = onclickAdd;
	}
	function onclickAdd(_) {
		trace("onclickAdd");
		needControlData.dispatch(this,CC);	
	}

	@:bind(this, ItemEvent.COMPONENT_CLICK_EVENT)
	override function onDeleteTableItem(e:ItemEvent) {
		super.onDeleteTableItem(e);
	}

	override public function populate(arg:Array<Automation>) {
		// reset();
		for (i in arg) {
			this.tableView.dataSource.add({
				val: i._value,
				defaultValue: i.defaultValue
			});
		}
		super.populate(null);
	}
	override public function validate(?force:Bool=false):Bool {
		data = [];
		if (super.validate() && !force)
			return true;
		else {
			var errors:Array<AutoErrors> = [];
			for (i in 0...tableView.dataSource.size) {

				var a = tableView.dataSource.get(i);
				// trace(a);
				try {
					data.push(new Automation(CC, a.val, a.defaultValue == "" ? "NULL" : a.defaultValue));
				} catch (e:Exception) {
					errors.push({
						row: i + 1,
						message: e.message,
						value: a.val,
						defaultValue: a.defaultValue,
						typ: CC
					});
				}
			}
			// trace(data,errors);
			if (errors.length > 0) {
				for (i in errors) {
					dispatchErrorNotif('CC AUTOMATION on row ${i.row}', 'CC Value must be a valid number between 0 and 119\nDefault bebtween 0 and 127.\nCurrent value:"${i.value}", default: "${i.defaultValue}"');
				}
				return false;
			} else if (tableView.dataSource.size > 0 && hasChanges) {
				dispatchNoErorNotification();
			}
			hasChanges = false;
			return true;
		}
	}

	public function setFromControl(control:Control, pot:types.HapaxEnums.Pot) {
		trace(control, pot);
		try {
			if(control.name == "BLANK")
				{
					onAddItem(null);
				}
				else{
					canIAdd.dispatch(this, true, {val: control.id, defaultValue: control.defaultValue});
				}
			// var ass:PotAssign = new PotAssign(new PotNumber(this.selectedIndex), pot, control.id, control.defaultValue);
			// setPot(ass);
		} catch (e:Exception) {
			trace(e.message);
		}
	}
}
