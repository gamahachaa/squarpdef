package views.components;

import haxe.Exception;
import haxe.ui.containers.TableView;
import haxe.ui.core.Screen;
import haxe.ui.events.ItemEvent;
import haxe.ui.events.MouseEvent;
import types.Binary128;
import types.LowRange;
import types.Name;
import types.Nrpn;
import utils.Converters;
import views.components.Controls.MyMenu;

@:build(haxe.ui.ComponentBuilder.build("assets/views/components/nrpn_control.xml"))
class NRPNControl extends Controls<Nrpn> {
	public var data:Map<String, Nrpn>;

	public function new() {
		super({
			name: "",
			cc: "",
			defaultValue: ""
		});
		getComps();
		data = new Map<String, Nrpn>();
	}

	// @:bind(this.tableView, MouseEvent.RIGHT_MOUSE_DOWN)
	// // @:bind(menuTarget2, MouseEvent.MOUSE_DOWN)
	// private function onMenuTarget(e:MouseEvent) {
	// 	var menu = new MyMenu(tableView);
	// 	menu.left = e.screenX;
	// 	menu.top = e.screenY;
	// 	Screen.instance.addComponent(menu);
	// }

	@:bind(add, MouseEvent.CLICK)
	function onAddItemInTable(e:MouseEvent) {
		addDefault();
	}

	// @:bind(tableView, ItemEvent.COMPONENT_CLICK_EVENT)
	// override function onDeleteTableItem(e:ItemEvent) {
	// 	trace(e.source.id, e.itemIndex);
	// 	if (e.source.id == "add")
	// 		super.onDeleteTableItem(e);
	// 	else if (e.source.id == "depth") {
	// 		/**
	// 		 * @todo chekc validation
	// 		 */
	// 		var item = tableView.dataSource.get(e.itemIndex);
	// 		// trace(item);
	// 	}
	// }

	@:bind(tableView, ItemEvent.COMPONENT_CLICK_EVENT)
	private function onMenuTarget(e:ItemEvent) {
		trace(e.source.id, e.itemIndex, e.data);
		if (e.source.id == "depth") {
			/**
			 * @todo chekc validation
			 */
			var item = tableView.dataSource.get(e.itemIndex);
			// trace(item);
		} else if(e.source.id == "add") {
			if(this.isDefault(e.data)){
				this.onDeleteTableItem(e);
			}else{
				trace(e.source.id, e.itemIndex, e.data);
				var menu = new MyMenu(this, e, NRPN);
				menu.left = e.source.screen.currentMouseX;
				menu.top = e.source.screen.currentMouseY;
				Screen.instance.addComponent(menu);
			}
			
		}
	}

	override public function validate(?force:Bool = false):Bool {
		// trace("validate start" );
		if (super.validate() && !force)
			return true;
		else {
			data = [];
			var noErrors = true;
			var nprn:Dynamic;

			if (tableView.dataSource != null) {
				for (i in 0...tableView.dataSource.size) {
					// trace("validate NRYPN", i);
					nprn = tableView.dataSource.get(i);
					try {
						if (nprn.name.trim() == "" || nprn.name == null)
							throw "Why create a NRPN control change withtout a name ?!? ";
						var depthIs14 = nprn.depth;
						var dfIsNull = nprn.defaultValue == null || nprn.defaultValue == "NULL" || nprn.defaultValue == "";
						if (!dfIsNull) {
							if (!Converters.IS_BETWEEN(nprn.defaultValue, 0, depthIs14 ? Converters.MAX_VALUE : 128)) {
								throw 'NRPN DEFAULT row ${i + 1} Depth is ${depthIs14 ? "14" : "7"} but default is ${nprn.defaultValue}';
							}
						}
						if (!depthIs14
							&& !(nprn.msb == "" || nprn.msb == "0" || nprn.msb.toUpperCase() == "NULL")
							&& Converters.IS_BETWEEN(nprn.lsb, 128, Converters.MAX_VALUE)) {
							throw 'NRPN LSB row ${i + 1} DEPTH is 7 and MSB is "${nprn.msb}" but LSB is "${nprn.lsb}".\nLSB can be over 127 only if DEPTH is 14 and MSB is 0 or NULL';
						}
						var item = new Nrpn(new Name(nprn.name), depthIs14, depthIs14 ? Left(new Binary128(nprn.lsb)) : Right(new LowRange(nprn.lsb)),
							nprn.msb, nprn.defaultValue);

						data.set(item.id, item);
					} catch (e:Exception) {
						// trace(nprn, e);
						noErrors = false;
						this.dispatchErrorNotif(' row ${i + 1}', '${e.message}');
					}
				}
				// trace(data, noErrors);
				if (!noErrors)
					return false;
			}
			if (hasChanges) {
				this.dispatchNoErorNotification();
			}
			hasChanges = false;
			return true;
		}
		return false;
	}
}
