package views.components;

import haxe.Exception;
import haxe.ui.components.Button;
import haxe.ui.containers.TableView;
import haxe.ui.core.ItemRenderer;
import haxe.ui.core.Screen;
import haxe.ui.events.ItemEvent;
import haxe.ui.events.MouseEvent;
import types.ControlChange;
import types.LowRange;
import types.Name;
import views.components.Controls.MyMenu;

using StringTools;

@:build(haxe.ui.ComponentBuilder.build("assets/views/components/cc_control.xml"))
class CCControl extends Controls<ControlChange> {
	public var data:Map<LowRange, ControlChange>;

	public function new() {
		super({
			name: "",
			cc: "",
			defaultValue: ""
		});
		getComps();
		data = new Map<LowRange, ControlChange>();
	}

	@:bind(add, MouseEvent.CLICK)
	function onAddItemInTable(e:MouseEvent) {
		addDefault();
	}

	// @:bind(tableView, ItemEvent.COMPONENT_CLICK_EVENT)
	// override function onDeleteTableItem(e:ItemEvent) {
	// 	trace(e.source.id);
	// 	super.onDeleteTableItem(e);
	// }
	// @:bind(this.tableView, MouseEvent.RIGHT_MOUSE_DOWN)

	@:bind(tableView, ItemEvent.COMPONENT_CLICK_EVENT)
	private function onMenuTarget(e:ItemEvent) {
		trace(e.source.id, e.itemIndex, e.data);
		if(e.source.id != "add") {
			return;
		}
		if (isDefault(e.data)) {
			this.onDeleteTableItem(e);
		} else {
			var menu = new MyMenu(this, e, CC);
			menu.left = e.source.screen.currentMouseX;
			menu.top = e.source.screen.currentMouseY;
			Screen.instance.addComponent(menu);
		}
	}

	override public function validate(?force:Bool = false):Bool {
		data = [];
		if (super.validate() && !force)
			return true;
		else {
			var noErrors = true;
			if (tableView.dataSource != null) {
				for (i in 0...tableView.dataSource.size) {
					try {
						var cc = tableView.dataSource.get(i);
						if (cc.name.trim() == "" || cc.name == null)
							throw "Why create a Control Change withtout a name ?!? ";
						var name:Name = new Name(cc.name, false);

						if (data.exists(cc.cc)) {
							throw 'Duplicated CC "${cc.cc}" ($name) with "${data.get(cc.cc).name} cc ${cc.cc}"';
						}
						data.set(cc.cc, new ControlChange(name, cc.cc, cc.defaultValue == "" ? "NULL" : cc.defaultValue));
						// ccDS.add(cc);
					} catch (e:Exception) {
						// trace(e);
						noErrors = false;
						dispatchErrorNotif('name on row ${i + 1}', '${e.message}');
					}
				}
				// data = ccDatasourceToMap(ccDS);
				if (!noErrors)
					return false;
				if (hasChanges) {
					dispatchNoErorNotification();
				}
			}
			hasChanges = false;
			return true;
		}
	}
}
