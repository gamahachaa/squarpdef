package views.components;

// import haxe.ui.components.Button;
import haxe.ui.components.Button;
import haxe.ui.events.MouseEvent;
import haxe.ui.styles.StyleSheet;
import types.MidiNote;
import views.components.PianorollView;

@:xml('
    <button icon="assets/img/piano.png" id="pianoicon" width="32" height="32"/>
')
class PianobtnView extends Button {
	//var roll:PianorollView;
	public var selectedNoteObject:Pitch;
	
	//var btn:Button;
//	public function new(roll:PianorollView) {
	public function new() {
        super();
		
		this.styleSheet = new StyleSheet();
		this.styleSheet.parse(".icon{margin:0;padding:0;}");
		//selectedNoteObject = PianorollView.NULLNoteObj;
		//this.roll = roll;
		iconPosition = "right";
		//updateLabel();
    }
	/*public function setSelectedNote(num:UInt)
	{
		this.selectedNoteObject = PianorollView.MIDINoteFromNumerical(num);
		updateLabel();
	}
	function updateLabel() {
		final m = selectedNoteObject?.musical.toUpperCase()??"";
		final n = selectedNoteObject?.numerical;
		//trace(n);
		text = m + " \n " + (n==-1?"":Std.string(n));
	}*/
    /*@:bind(this, MouseEvent.CLICK)
    function onBtnClicked(e:MouseEvent) {
		//var col:Column = cast(e.target, Column);
		//trace( e.target.id, e.target.value, e.target.parentComponent.parentComponent.value);
		//trace(this.id);
		//trace(this.roll.hidden);
		if(this.roll.hidden){
			this.roll.selectedNoteObject = selectedNoteObject;
			this.roll.noteSignal.add(onPianoChanged);
			this.roll.showSelectedNote();
		}

		//roll.show();
    }
	//@:bind(roll, UIEvent.CHANGE)
	/*function onPianoChanged(e:MidiNote) {
		//selectedNoteObject = roll.selectedNoteObject;
		//this.roll.noteSignal.remove(onPianoChanged);
		selectedNoteObject = e;
		updateLabel();
	}*/
	
}