package views;

import haxe.ui.components.DropDown;
import haxe.ui.components.Label;
import haxe.ui.components.TextArea;
import haxe.ui.components.TextField;
import haxe.ui.containers.Box;
import haxe.ui.containers.VBox;
import haxe.ui.containers.dialogs.Dialog;
import haxe.ui.containers.menus.Menu;
import haxe.ui.containers.menus.MenuItem;
import haxe.ui.containers.windows.WindowManager;
import haxe.ui.core.Screen;
import haxe.ui.events.ItemEvent;
import haxe.ui.events.MenuEvent;
import haxe.ui.events.MouseEvent;
import haxe.ui.events.UIEvent;
import haxe.ui.notifications.NotificationManager;
import haxe.ui.notifications.NotificationType;
import models.HapaxDefinition;
import signals.Signal1;
import signals.Signal2;
import types.*;
import types.HapaxEnums;
import views.components.Paster;
import views.windows.*;
import views.windows.WindowControls.DataControls;

using Lambda;
using StringTools;
using haxe.ui.animation.AnimationTools;

#if js
import js.Browser;
import utils.Google;
#end

typedef Auto = {
	var typ:Dynamic;
	var val:String;
	var defo:String;
}

/**
 * Main edditor view
 */
@:build(haxe.ui.ComponentBuilder.build("assets/views/hapaxEditor-view.xml"))
class HapaxEditorView extends ManageFileView {
	public var addDoc:Bool;
	public var winPots:WindowPots;
	public var winAuto:WindowAutomations<Automation>;
	public var winPc:WindowPc<ProgramChange>;
	public var winDl:WindowDrumlane<DrumLane>;
	public var winControls:WindowControls<Control>;
	public var pasted:Signal1<String>;

	var tabs:Array<IValidatable<Dynamic>>;
	var selectedTab:String;
	var browserFile:BrowseFiles;
	var google:Google;
	var product:String;
	var brand:String;

	public function new() {
		super();
		// var blankCCControl = new Control(new Name("BLANK"), "0", "NULL");
		
		pasted = new Signal1<String>();
		this.addDoc = false;
		this.google = new Google();
		selectedTab = "";
		this.product = "";
		this.brand = "";
		loadComponents();
		#if !js
		cast(this.print, MenuItem).disabled = true;
		#end
	}

	public function setUpGoogleButton(product:String, ?brand:String = "") {
		this.brand = brand;
		this.product = product;
		var b = brand == "" ? "" : '\nfrom "$brand"';
		this.searchGoogle.text = '"$product"$b\nuser manual';
		this.searchGoogle.tooltip = 'Search google for a user guide of "$product" $b';
		searchGoogle.hidden = brand.trim() == "" && product.trim() == "";
	}

	public function populate(def:HapaxDefinition) {
		resetAll();
		TRACKNAME.text = def.trackName;
		COMMENT.text = def.comment;
		INCHAN.selectedItem = def.inchan;
		INPORT.selectedItem = def.inport;
		OUTCHAN.selectedItem = def.outchan;
		OUTPORT.selectedItem = def.outport;
		TYPE.selectedItem = def.trackType;
		winAuto.MAXRATE.selectedItem = def.maxrate;

		winAuto.populate(def.automation.array());
		winPots.populate(def.potAssign.array());
		winPc.populate(def.pc.array());
		winDl.populate(def.drumlanne.array());
		// prepareForPopulation
		winControls.prepareForPopulation({
			ccs: def.cc,
			ccps: def.ccp,
			nrpn: def.nrpn
		});
	}

	@:bind(TRACKNAME, UIEvent.CHANGE)
	function ontextChanged(e:UIEvent) {
		trace(TRACKNAME.focus);
		if (TRACKNAME.focus) {
			setUpGoogleButton(TRACKNAME.text, brand);
		}
	}

	@:bind(searchGoogle, MouseEvent.CLICK)
	function search(e:MouseEvent) {
		// trace(this.brand, this.product);
		if (this.brand == "" && this.product == "")
			return;
		else
			this.google.search(this.brand, this.product);
	}

	@:bind(myMenu, MenuEvent.MENU_SELECTED)
	function onMenu(e:MenuEvent) {
		switch (e.menuItem.id) {
			case "newDef":
				resetAll();
			case "browse":
				onBrowseFileSelected();
			case "load":
				onOpenFile(null);
			case "converter":
				convert();
			case "savedoc":
				this.addDoc = this.savedoc.selected;
			case "save":
				onSaveTextFile();
			case "saveas":
				onSaveTextFile(true);
			case "export":
				onSaveTextFile(false, false);
			case "midiset":
				Dialoging.showInfo("Midi set up", "Comming soon ...");
			case "kb":
				Dialoging.showInfo("Midi keyboard player", "Comming soon ...");
			case "about":
				Dialoging.showInfo("About",
					macros.Version.getLatestTag() +
					"\nUnofficial definition file manager.\nSee 'contribute' to get in touch, report bugs, request or participate to enhancements");
			#if js
			case "contribute":
				Browser.window.open("https://codeberg.org/gamahachaa/squarpdef");

			case "doc":
				Browser.window.open("https://squarp.net/hapax/manual/modetrack/#XX7CT5W++");

			case "print":
				// this.printSignal.dispatch();
				doPrintJob();
			#else
			#end
			case _:
				return;
		}
	}

	@:bind(OUTPORT, UIEvent.CHANGE)
	function onOutportChanged(e:UIEvent) {
		final isMidi = HapaxEnums.IS_midi_outport_index(cast(e.target, DropDown).selectedIndex);
		if (isMidi) {
			OUTCHAN.disabled = false;
		} else {
			OUTCHAN.selectedIndex = 0;
			OUTCHAN.disabled = true;
		}
	}

	function fileBrowsed(brand:String, product:String, content:String) {
		trace(brand, product);

		WindowManager.instance.minimizeWindow(browserFile);
		this.fileLoaded.dispatch({
			{
				text: content,
				isBinary: false,
				name: brand + "__" + product
			}
		});

		// browserFile.reset();
	}

	function onBrowseFileSelected() {
		if (WindowManager.instance.windows.indexOf(browserFile) == -1) {
			browserFile = new BrowseFiles();
			browserFile.browseSignal.add(fileBrowsed);
			WindowManager.instance.addWindow(browserFile);
		} else {
			WindowManager.instance.restoreWindow(browserFile);
		}
	}

	// @:bind(openFile, MouseEvent.CLICK)
	private override function onOpenFile(_) {
		super.onOpenFile(_);
	}

	private function onSaveTextFile(newName:Bool = false, ?hapax:Bool = true) {
		var hasError = !validateAll();
		var error = "";
		var filename = "";
		if (hasError) {}
		if (hasError) {
			NotificationManager.instance.addNotification({
				title: "Correct the erros before submiting",
				body: error,
				type: NotificationType.Error,
				expiryMs: -1
			});
		} else {
			NotificationManager.instance.clearNotifications();
			try {
				final trackName = TRACKNAME.text.trim();
				if (trackName == "NULL" || trackName == "" || newName) {
					var dialog = new SaveasDialog();

					dialog.onDialogClosed = function(e:DialogEvent) {
						if (e.button == SaveasDialog.SAVE)
							saveSignal.dispatch(dialog.filename.text, hapax);
					}
					dialog.showDialog();
				} else {
					saveSignal.dispatch("", hapax);
				}
			} catch (e) {
				error += e.message;
			}
		}
	}

	/**
	 * initialize all components
	 */
	function loadComponents() {
		this.savedoc.selected = this.addDoc;
		// Track Type
		for (i in HapaxEnums.TrackType) {
			this.TYPE.dataSource.add(i);
		}
		// In port
		for (i in HapaxEnums.Inport) {
			INPORT.dataSource.add(i);
		}

		// Out port
		for (i in HapaxEnums.Outport) {
			OUTPORT.dataSource.add(i);
		}

		// Outchan
		for (i in Channel.ALL) {
			OUTCHAN.dataSource.add(i);
		}

		// inchan
		for (i in Channel.ALL) {
			INCHAN.dataSource.add(i);
		}
		tabs = [];
		winAuto = new WindowAutomations<Automation>();
		winAuto.CC_AUTO.needControlData.add(onControlDataRequested);
		winAuto.NRPN_AUTO.needControlData.add(onControlDataRequested);
		winPc = new WindowPc<ProgramChange>();
		winPots = new WindowPots();

		winDl = new WindowDrumlane<DrumLane>();
		winControls = new WindowControls<Control>();
		winControls.CC.sharingControlData.add(transferControls);
		winControls.NRPN.sharingControlData.add(transferControls);
		tabs.push(winPots);
		tabs.push(winAuto);
		tabs.push(winPc);
		tabs.push(winDl);
		tabs.push(winControls);

		var num = 0;
		for (v in tabs) {
			mainTabView.addComponent(cast(v));
			changeStatusIcon(cast(v));
		}
		this.mainTabView.onChange = (e) -> validateTab(mainTabView.selectedPage.id);
		winPots.needControlData.add(onControlDataRequested); //
	}
	function transferControls(e:ItemEvent, action:String, type:Pot){
		var data = e.data;
		var win:ITransferable;
		trace(data);
		switch(action){
			case "toAuto": win = type == CC ? cast winAuto.CC_AUTO: cast winAuto.NRPN_AUTO;
			case _: {
				winPots.selectedIndex = action.substr(3);
				win = cast winPots;
			}
				
		}
		var control:Control = cast switch (type){
			case CC :new ControlChange(
				new Name(data.name),
				new LowRange(data.cc),
				data.defaultValue
			);
			case _: new Nrpn(
				new Name(data.name),
				data.depth,
				data.depth ? Left(new Binary128(data.lsb)) : Right(new LowRange(data.lsb)) ,
				new LowRange(data.msb),
				data.defaultValue
			);
		}
		win.setFromControl(control, type);
	}
	/**
	 * @todo interfaced parameter
	 * @param id 
	 * @param pot 
	 */
	function onControlDataRequested(tranferable:ITransferable, pot:Pot) {
		this.winControls.onValidate(true);
		if(pot == CC && winControls.data.ccs.empty()){
			tranferable.setFromControl(ControlChange.BLANK, CC);
		}else if(pot == NRPN && winControls.data.nrpn.empty()){
			tranferable.setFromControl(Nrpn.BLANK, NRPN);
		}
		else{
			var menu = new ControlDataMenu(winControls.data, pot);
			menu.menuSelectecSignal.add(tranferable.setFromControl);
			menu.left = Screen.instance.currentMouseX;
			menu.top = Screen.instance.currentMouseY;
			Screen.instance.addComponent(menu);
		}
		
	}

	function doPrintJob() {
		var hasError = !validateAll();
		this.printSignal.dispatch();
	}

	function convert() {
		var dialog = new Paster(pasted);
		// pasteContainer = dialog.content;
		dialog.onDialogClosed = function(e:DialogEvent) {
			if (e.button == DialogButton.OK) {
				var info = {
					text: dialog.content.text,
					isBinary: false,
					name: ""
				}
				this.fileLoaded.dispatch(info);
			}
		}
		dialog.showDialog();
	}

	function validateTab(tabId:String) {
		NotificationManager.instance.maxNotifications = 5;
		final currentTab:IValidatable<Dynamic> = cast this.findComponent(selectedTab);
		if (selectedTab != "") {
			currentTab.onValidate();
			changeStatusIcon(currentTab);
		}
		selectedTab = tabId;
	}

	function changeStatusIcon(tab:IValidatable<Dynamic>) {
		var t:Box = cast tab;
		if (tab.isValid) {
			t.icon = "assets/img/bullet_green.png";
		} else {
			t.icon = "assets/img/bullet_red.png";
		}
	}

	function resetAll() {
		trace("resetAll");
		// trace(this.brand, this.product);
		brand = "";
		product = "";
		searchGoogle.hidden = true;
		TRACKNAME.text = "";
		COMMENT.text = "";
		INCHAN.selectedIndex = 0;
		INPORT.selectedIndex = 0;
		OUTCHAN.selectedIndex = 0;
		OUTPORT.selectedIndex = 0;
		TYPE.selectedIndex = 0;
		for (i in tabs)
			i.reset();
	}

	function validateAll() {
		var allGood = true;
		for (i in tabs) {
			i.onValidate(true);
			allGood = allGood && i.isValid;
			changeStatusIcon(i);
		}

		return allGood;
	}

	function isInitState():Bool {
		for (i in tabs) {
			if (!i.initState)
				return false;
		}
		return true;
	}
}

@:build(haxe.ui.macros.ComponentMacros.build("assets/views/components/saveas.xml"))
class SaveasDialog extends Dialog {
	public static final SAVE:String = "Save";

	public function new() {
		super();
		buttons = DialogButton.CANCEL | SAVE;
		defaultButton = SAVE;
		this.title = "Save as ...";
	}

	public override function validateDialog(button:DialogButton, fn:Bool->Void) {
		var valid = true;
		var errorText = "Filename should not be blank";
		if (button == SAVE) {
			if (filename.text == "" || filename.text == null || Regex.urlReserved.match(filename.text)) {
				filename.flash();
				valid = false;
				if (Regex.urlReserved.match(filename.text)) {
					errorText = "Avoid the folowing caharacters\n ! * ' ( ) ; : @ &amp; = + $ , / ? % # [ ]";
				}
			}
			if (valid == false) {
				NotificationManager.instance.addNotification({
					title: "Problem with the file name!",
					body: errorText,
					type: NotificationType.Error
				});
				this.shake();
			} else {
				NotificationManager.instance.addNotification({
					title: "Good name choice !",
					body: "Saving the definition as '" + filename.text + "'!",
					type: NotificationType.Success
				});
			}
		}
		fn(valid);
	}
}

class ControlDataMenu extends Menu {
	// var pot:WindowPots;
	public var menuSelectecSignal:Signal2<Control, Pot>;

	public function new(data:DataControls, ?filter:Pot = NULL) {
		super();
		menuSelectecSignal = new Signal2<Control, Pot>();

		var blankCCLeaf = makeLeaf(ControlChange.BLANK, CC);
		var blankNRPNLeaf = makeLeaf(Nrpn.BLANK, NRPN);

		if (!data.ccs.empty() && (filter == NULL ||filter == CC)) {
			var ccsMenu = new Menu();
			ccsMenu.id = ccsMenu.text = "Add CC here ...";
			var container:Menu = if(filter == CC){
				this;
			}else{
				ccsMenu;
			}
			
			container.addComponent(blankCCLeaf);
			var tabs = Control.buildAphabetTree(cast data.ccs);
			for (alpha => ccs in tabs) {
				var alphaMenu = new Menu();
				alphaMenu.id = alphaMenu.text = alpha;
				for (id => cc in ccs) {
					// trace(id, cc);
					var leaf = makeLeaf(data.ccs.get(new LowRange(cc.id)), CC);
					alphaMenu.addComponent(leaf);
				}
				container.addComponent(alphaMenu);
			}
			if(container != this) this.addComponent(container);
		}
		else if (filter == NULL) {
			this.addComponent(blankCCLeaf);
		}
		if (!data.nrpn.empty() && (filter == NULL ||filter == NRPN)) {
			var nrpnMenu = new Menu();
			nrpnMenu.id = nrpnMenu.text = "Add NRPN here ...";
			var container:Menu = if(filter == NRPN){
				this;
			}else{
				nrpnMenu;
			}
			
			container.addComponent(blankNRPNLeaf);
			var tabs = Control.buildAphabetTree(cast data.nrpn);
			for (alpha => ns in tabs) {
				var alphaMenu = new Menu();
				alphaMenu.id = alphaMenu.text = alpha;
				for (id => n in ns) {
					// trace(id, cc);
					var leaf = makeLeaf(data.nrpn.get(new LowRange(n.id)), NRPN);
					alphaMenu.addComponent(leaf);
				}
				container.addComponent(alphaMenu);
			}
			if(container != this) this.addComponent(container);
		}
		else if (filter == NULL) {
			this.addComponent(blankNRPNLeaf);
		}
		// if (filter == NULL || filter == NRPN) {
		// 	this.addComponent(nrpnMenu);
		// }
		// this.onMenuSelected = (e:MenuEvent)->trace(e.menuItem.userData, e.menuItem.id);
		this.onMenuSelected = (e:MenuEvent) -> (menuSelectecSignal.dispatch(e.menuItem.userData,
			Type.createEnum(Pot, e.menuItem.id.substring(e.menuItem.id.lastIndexOf("#") + 1))));
	}

	function makeLeaf(item:Dynamic, pot:Pot) {
		// trace(item);
		var m = new MenuItem();

		m.id = item.id + "#" + pot;
		m.text = '${item.name} (${item.id})';
		m.userData = item;
		return m;
	}
}
