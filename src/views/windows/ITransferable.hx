package views.windows;

import signals.Signal2;
import types.Control;

interface ITransferable {
    public function setFromControl(control:Control, pot: types.HapaxEnums.Pot):Void;
    public var needControlData:Signal2<ITransferable, types.HapaxEnums.Pot>;
}