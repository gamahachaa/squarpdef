package views.windows;

import haxe.ui.containers.HBox;
// import haxe.ui.containers.Splitter;
import haxe.ui.notifications.NotificationManager;
import signals.Signal.Signal0;
import types.ControlChange;
import types.ControlChangePair;
import types.LowRange;
import types.Nrpn;

using Lambda;

typedef DataControls = {
	var ccs:Map<LowRange, ControlChange>;
	var ccps:Map<String, ControlChangePair>;
	var nrpn:Map<String, Nrpn>;
}

@:build(haxe.ui.ComponentBuilder.build("assets/views/controls.xml"))
// class WindowControls<T> extends HBox implements IValidatable<T> {
class WindowControls<T> extends HBox implements IValidatable<T> {
	// public var dataSignal:Signal1<Controls>;
	public var hasChanges:Bool;
	public var initState:Bool;
	public var isValid:Bool;
	public var data:DataControls;
	public var justValidatedSignal:Signal0;
	public function new() {
		super();
		justValidatedSignal = new Signal0();
		isValid = true;
		initState = true;
		data = {
			ccs:[],
			ccps:[],
			nrpn:[]
		};
	}

	public function reset() {
		this.CC.reset();
		this.CC_PAIRS.reset();
		this.NRPN.reset();
	}
	public function prepareForPopulation(c:DataControls) {
		reset();
		this.CC.populate(c.ccs.array());
		this.CC_PAIRS.populate(c.ccps.array());
		this.NRPN.populate(c.nrpn.array());
		populate(null);
	}
	public function populate(arg:Array<T>) {
		
		// var ccs = arg.filter((e)->(Std.isOfType(e, ControlChange)));
		// var ccps = arg.filter((e)->(Std.isOfType(e, ControlChangePair)));
		// var nrpns = arg.filter((e)->(Std.isOfType(e, Nrpn)));
		// this.CC.populate(cast(ccs));
		// this.CC_PAIRS.populate(cast(ccps));
		// this.NRPN.populate(cast(nrpns));
		//setData();
	}

	

	public function onValidate(?force:Bool=false) {
		NotificationManager.instance.clearNotifications();
		isValid = validate(force);
		if(isValid) justValidatedSignal.dispatch();
	}
	function validate(?force=false):Bool {
		var ccValid = CC.validate(force);
		var ccpValid = CC_PAIRS.validate(force);
		var nrpnValid = NRPN.validate(force);
		this.initState = CC.initState && CC_PAIRS.initState && NRPN.initState;
		this.hasChanges = CC.hasChanges && CC_PAIRS.hasChanges && NRPN.hasChanges;
		setData();
		return ccValid && ccpValid && nrpnValid;
	}
	function setData() {
		data.ccs = CC.data;
		data.ccps = CC_PAIRS.data;
		data.nrpn = NRPN.data;
	}
	
}
