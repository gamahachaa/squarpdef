package views.windows;

import haxe.crypto.Md5;
import haxe.ui.containers.HBox;
import haxe.ui.containers.TreeView;
import haxe.ui.containers.windows.Window;
import haxe.ui.events.MouseEvent;
import signals.Signal3;
import utils.StringToolsBB;

using Lambda;
using StringTools;

#if js
import haxe.http.HttpJs;
import js.Browser;
#end
/**
 * Dialog to list communtity saved definition files
 */
class BrowseFiles extends Window{
    var fileList:String;
    var treeHigh:TreeView;
    var treeLow:TreeView;
    var root:String;
    var container:HBox;
    var path:String;

    public var browseSignal:Signal3<String,String,String>;
    public function new() {
        super();
        container = new HBox();
        this.addComponent(container);
        browseSignal = new Signal3<String,String,String>();
        this.width = 1024;
        this.height = 600;
        container.percentHeight =100;
        container.percentWidth =100;
        this.title = "Browse exiting files shared by the community";
        
        treeHigh = new TreeView();
        treeHigh.id = "treeHigh";
        treeLow = new TreeView();
        treeLow.id = "treeLow";

        treeLow.percentWidth =  treeHigh.percentWidth = 50;
        treeLow.percentHeight = treeHigh.percentHeight = 100;
        treeHigh.onClick = treeLow.onClick = selectFile;
        #if js
        path = "/";
        #if debug
        root = Browser.location.href.replace("index_debug.html","") ;
        #else
        root = Browser.location.href.replace("index.html","") ;
        #end
        var u = new HttpJs(root + path + "list.txt" + "?rnd=" + Md5.encode(Date.now().toString()));// prevent caching
         trace(root + path + "list.txt", u.url);
        u.onData = buildTree;
        u.onError = (e)->trace(e);
        u.request();
        
        #end
       
       
    }
    public function reset() {
        this.treeLow.selectedNode = this.treeHigh.selectedNode = null;
        
    }
    function selectFile(e:MouseEvent) {
        //trace(e.target.id);
        var curTree:TreeView = cast(e.target, TreeView);
        var prevTree:TreeView = cast(e.target.id == "treeHigh" ? treeLow: treeHigh , TreeView);
        prevTree.selectedNode = null;
        if(curTree.selectedNode == null ||  curTree.selectedNode.data == null ) return;
        var d = curTree.selectedNode.data;
        var brand = curTree.selectedNode.parentNode?.data.text;
        // trace(curTree.selectedNode.parentNode.data);
        if(d.link != "#") {
            #if js
            var h:HttpJs = new HttpJs(d.link);
            h.onData = (e)->browseSignal.dispatch(brand, d.text, e);
            h.request();
            #end
        }
        
    }
    function buildTree(data:String){
        this.fileList = data;
        var hapaxList = fileList.split("\n")
            .filter((i)->(i != "" && i.indexOf("definitions/hapax/") > -1 && i.indexOf(".DS_Store") == -1));
        hapaxList.sort(
            (a,b)->(a.toLowerCase()==b.toLowerCase()?0:a.toLowerCase()>b.toLowerCase()?1:-1)
            );
        var d = createDataMap(
            hapaxList.map((e)->( e.trim().split("/")))
            .filter((e)->(e.length ==4))
            
        );
        treeLow.clearNodes();
        treeHigh.clearNodes();
        var size = Math.ceil(d.count()/2 + 1);
        var pivot = "";
        var keys = d.keys();
        //trace(keys);
        while (size>0) {
            pivot = keys.next(); 
            size--;
        };// (`.`)...
        // trace(pivot);
        var t:TreeView;
        for(letter=>alphabet in d){
            t = letter < pivot ?treeLow:treeHigh;
            var a = t.addNode({text:letter,link:"#"});
            for(brand=>files in alphabet){
                var b = a.addNode({text:brand,link:"#"});
                for(title =>file in files){
                    var leaf = b.addNode({text:title,link:file});
                }
            }
        }
        container.addComponent(treeLow);
        container.addComponent(treeHigh);
    }
    function  createDataMap(tab:Array<Array<String>>):Map<String,Map<String,Map<String, String>>>{
        var m:Map<String,Map<String,Map<String, String>>> = [];
        // trace(tab);
        //tab.sort((a,b)->(a.toLowerCase()==b.toLowerCase()?0:a.toLowerCase()>b.toLowerCase()?1:-1));//sort alphabet
        for(i in tab){
            // trace(i);
            //if(i == "" || i.indexOf("definitions/hapax/")== -1|| i.indexOf(".ds_store")> -1) continue;

            //var line = i.trim().split("/");
            //if(line.length !=4) continue; //skip lost text files

            var brand = StringToolsBB.filenameToTitle(i[2]);
            var alphabet = brand.substr(0,1).toUpperCase();
            var file = i[3];
            var fileTitle = StringToolsBB.filenameToTitle(file.replace(".txt",""));
            var link = root + path + i.join("/");
            if(!m.exists(alphabet)) m.set(alphabet, []);
            if(!m.get(alphabet).exists(brand)) m.get(alphabet).set(brand,[]);
            m.get(alphabet).get(brand).set(fileTitle, link);
        }
        // trace(m);
        return m;
    }
    // function  createDataMap(tab:Array<String>):Map<String,Map<String,Map<String, String>>>{
    //     var m:Map<String,Map<String,Map<String, String>>> = [];
    //     tab.sort((a,b)->(a.toLowerCase()==b.toLowerCase()?0:a.toLowerCase()>b.toLowerCase()?1:-1));//sort alphabet
    //     for(i in tab){
    //         //  trace(i);
    //         if(i == "" || i.indexOf("definitions/hapax/")== -1|| i.indexOf(".ds_store")> -1) continue;
    //         //trace(i);
    //         var line = i.trim().split("/");
    //         if(line.length !=4) continue; //skip lost text files
    //         // trace(line);
    //         var brand = StringToolsBB.filenameToTitle(line[2]);
    //         var alphabet = brand.substr(0,1).toUpperCase();
    //         var file = line[3];
    //         var fileTitle = StringToolsBB.filenameToTitle(file.replace(".txt",""));
    //         var link = root + path + i;
    //         if(!m.exists(alphabet)) m.set(alphabet, []);
    //         if(!m.get(alphabet).exists(brand)) m.get(alphabet).set(brand,[]);
    //         m.get(alphabet).get(brand).set(fileTitle, link);
    //     }
    //     // trace(m);
    //     return m;
    // }


}