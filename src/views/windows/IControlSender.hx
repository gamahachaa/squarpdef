package views.windows;

import haxe.ui.events.ItemEvent;
import signals.Signal3;
import types.HapaxEnums.Pot;

interface IControlSender {
    public var sharingControlData:Signal3<ItemEvent, String, Pot>;
    public function deleteItem(e:ItemEvent):Void;
    // public function getDefaultNull():Dynamic;
}