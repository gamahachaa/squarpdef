package views.windows;

import haxe.ui.containers.TableView;
import haxe.ui.containers.VBox;
import haxe.ui.events.ItemEvent;
import haxe.ui.notifications.NotificationManager;
// import haxe.ui.notifications.NotificationType;
import signals.Signal.Signal0;
import utils.Notificator;

using Lambda;
/**
 * Parent class for custom table views
 */
class BaseInteractiveTables<T> extends VBox implements IValidatable<T>{
    var tableView:TableView;
	public var isValid:Bool;
	public var hasChanges:Bool;
	public var initState:Bool;
	public var justValidatedSignal:Signal0;
    public function new() {
        super();
		justValidatedSignal = new Signal0();
		isValid = true;
		hasChanges= false;
    }
	public function count():Int{
		return this.tableView.dataSource.size;
	}
    public function reset() {
		this.tableView.dataSource.clear();
		
	}
    public function getComps(){
        tableView = this.findComponents("interactiveTable",TableView)[0];
    }
	public function onValidate(?force:Bool=false) {
        NotificationManager.instance.clearNotifications();
		isValid = validate(force);
		if(isValid) justValidatedSignal.dispatch();
	}
    function validate(?force:Bool=false):Bool {
        if (tableView.dataSource == null || initState)
			return true;
		return false;
    }

    /**
     * @todo shoudl use T reflection
     * @param item 
     */
    public function addItemInTable(item:Dynamic) {
		initState = false;
		hasChanges = true;
		tableView.selectedIndex = tableView.dataSource.add(item);
	}
	public function checkUnique(item:Dynamic, ?filter:Array<String>=null):Int{
		trace(item, filter);
		var itemFields = Reflect.fields(item);
		// trace(itemFields);
		for (s in 0...tableView.dataSource.size) {
			var i = tableView.dataSource.get(s);
			trace(i);
			var iFields = Reflect.fields(i);
			// trace(iFields);
			if(itemFields.length != iFields.length) throw "Cannot check unicity. Different field count ";
			if(!itemFields.foreach((e)->(iFields.indexOf(e)>-1))) throw "Cannot check unicity. Different fields";
			if(filter !=null && filter.length>0) {
				if( !filter.foreach((e)->(Reflect.hasField(i, e)) )) throw "Cannot check unicity. Filter not in datasource item";
				else{
					trace(filter.foreach((e)->(Reflect.field(i, e)) == Reflect.field(item, e)));
					if(filter.foreach((e)->(Reflect.field(i, e)) == Reflect.field(item, e)))  return s;
				}
			}
			else{
				if(itemFields.foreach((e)->( Reflect.field(i, e) == Reflect.field(item, e) ))) return s;
			}
			
		}
		return -1;		
	}
    function onDeleteTableItem(e:ItemEvent) {
		initState = false;
		hasChanges = true;
		if (tableView != null && e.source.id == "add") {
			tableView.dataSource.removeAt(e.itemIndex);
			tableView.selectedIndex = -1;
		}
	}
	function dispatchNoErorNotification(){
		Notificator.dispatchNoErorNotification(this.text);
	}
	function dispatchErrorNotif(title:String, body:String){
		Notificator.dispatchErrorNotif(this.text, title, body);
	}
	public function populate(arg:Array<T>) { 
		initState = true;
	}

}