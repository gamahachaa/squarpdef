package views.windows;

import haxe.Exception;
import haxe.ui.components.CheckBox;
import haxe.ui.containers.HBox;
import haxe.ui.events.UIEvent;
import haxe.ui.notifications.NotificationManager;
import signals.Signal.Signal0;
import types.Automation;
import types.HapaxEnums.Pot;
import types.Rate;
import utils.Notificator;
import views.components.Automations;

using Lambda;
typedef Auto = {
	var typ:Dynamic;
	var val:String;
	var defo:String;
}

typedef AutoErrors = {
	var row:Int;
	var message:String;
	var value:String;
	var typ:Pot;
	var ?defaultValue:Dynamic;
}

@:build(haxe.ui.ComponentBuilder.build("assets/views/automations.xml"))
class WindowAutomations<T> extends HBox implements IValidatable<T> {
	static final ERRORS:Map<Pot, String> = [
		CC => "CC Value must be a valid number between 0 and 119",
		PB => "PB and AT, any text after the TYPE will be ignored",
		CV => "CV, value must be between 1 and 4,\nDEFAULT_VALUE must be either a valid number between 0 and 65535, or a voltage between -5V and 5V, e.g. -4.25V or 1.7V",
		NRPN => "NRPN, value must be MSB:LSB:DEPTH, \nwith both lsb & msb bebtween 0 and 127, and DEPTH being either 7 or 14"
	];
	public var justValidatedSignal:Signal0;
	public var data:Array<Automation>;

	var lanesCount:Int;

	public var hasChanges:Bool;
	public var initState:Bool;
	public var isValid:Bool;

	static final MAX_LANES:Int = 64;

	public function new() {
		super();
		justValidatedSignal = new Signal0();
		isValid = true;
		initState = true;
		lanesCount = 0;
		// getComps();
		data = [];
		for (i in Rate.RATE) {
			MAXRATE.dataSource.add(i);
		}
		this.CC_AUTO.canIAdd.add(checkCountForTables);
		this.NRPN_AUTO.canIAdd.add(checkCountForTables);
	}
	public function getMaxRate():Rate{
		return new Rate(this.MAXRATE.value);
	}
	function checkCountForTables(e:Automations<Automation>, adding:Bool, item:Dynamic) {
		trace(e.id, item);
		NotificationManager.instance.clearNotifications();
		if (adding) {
			if (countPassed()) {
				if(item== null)
					e.addDefault();
				else{
					//trace(e.checkUnique(item));
					var filter = switch(e.id) {
						case "NRPN_AUTO" :["msb", "lsb", "depth"];
						case _: ["val"];
					}
					var result = e.checkUnique(item,filter);
					var varDesc = filter.fold((e, first)->(first + Reflect.field(item,e)+ " "  ),"");
					trace(filter, varDesc);
					if(result == -1){
						e.addItemInTable(item);
						Notificator.dispatchSuccessNotif("Successfully added automation", varDesc);
					}
					else{
						Notificator.dispatchWarnNotif("Cannot add automation DUPLICATE", '$varDesc is already there near row ${result+1}');
					}
				}
			} else {
				Notificator.dispatchWarnNotif("Cannot add automation", 'Max cout $MAX_LANES reached');
			}
		}
		updateCount();
	}

	@:bind(autoat, UIEvent.CHANGE)
	@:bind(autopb, UIEvent.CHANGE)
	@:bind(autocv1, UIEvent.CHANGE)
	@:bind(autocv2, UIEvent.CHANGE)
	@:bind(autocv3, UIEvent.CHANGE)
	@:bind(autocv4, UIEvent.CHANGE)
	function onUiChanged(e:UIEvent) {
		
		if (!countPassed()) {
			cast(e.target, CheckBox).selected = false;
		}

		updateCount();
	}

	inline function countPassed():Bool {
		return lanesCount < MAX_LANES;
	}

	function updateCount() {
		NotificationManager.instance.clearNotifications();
		lanesCount = this.CC_AUTO.count() + this.NRPN_AUTO.count();
		lanesCount += this.autoat.selected ? 1 : 0;
		lanesCount += this.autopb.selected ? 1 : 0;
		lanesCount += this.autocv1.selected ? 1 : 0;
		lanesCount += this.autocv2.selected ? 1 : 0;
		lanesCount += this.autocv3.selected ? 1 : 0;
		lanesCount += this.autocv4.selected ? 1 : 0;

		autocv4.disabled = !autocv4.selected && !countPassed();
		autocv3.disabled = !autocv3.selected && !countPassed();
		autocv2.disabled = !autocv2.selected && !countPassed();
		autocv1.disabled = !autocv1.selected && !countPassed();
		autoat.disabled = !autoat.selected && !countPassed();
		autopb.disabled = !autopb.selected && !countPassed();
		automationCount.text = '${lanesCount} / $MAX_LANES max automations';
		hasChanges = true;
	}

	public function onValidate(?force:Bool=false) {
		NotificationManager.instance.clearNotifications();
		isValid = validate(force);
		if(isValid) justValidatedSignal.dispatch();
	}

	public function reset() {
		CC_AUTO.reset();
		NRPN_AUTO.reset();
		this.autocv1.selected = false;
		this.autocv1defo.text = "";
		this.autocv2.selected = false;
		this.autocv2defo.text = "";
		this.autocv3.selected = false;
		this.autocv3defo.text = "";
		this.autocv4.selected = false;
		this.autocv4defo.text = "";
		this.autopb.selected = false;
		this.autoat.selected = false;
	}

	public function populate(arg:Array<T>) {
		trace(arg);
		// reset();
		try {
			this.MAXRATE.selectedItem = "192";
			var autos:Array<Automation> = cast(arg);
			var ccAutos = autos.filter((e:Automation)->(e.automationType == CC));
			var nrpnAutos = autos.filter((e:Automation) -> (e.automationType == NRPN));
			trace(ccAutos);
			trace(nrpnAutos);
			CC_AUTO.populate(ccAutos);
			NRPN_AUTO.populate(nrpnAutos);
			// var nrpnAutos = autos.filter((e:Automation)->(e.automationType == NRPN));
			var cvAutos = autos.filter((e:Automation) -> (e.automationType == CV));
			for (i in cvAutos) {
				switch (i._value) {
					case "1":
						{
							this.autocv1.selected = true;
							this.autocv1defo.text = i.defaultValue;
						}
					case "2":
						{
							this.autocv2.selected = true;
							this.autocv2defo.text = i.defaultValue;
						}
					case "3":
						{
							this.autocv3.selected = true;
							this.autocv3defo.text = i.defaultValue;
						}
					case "4":
						{
							this.autocv4.selected = true;
							this.autocv4defo.text = i.defaultValue;
						}
					case _:
						break;
				}
			}
			var pbAuto = autos.filter((e:Automation) -> (e.automationType == PB));
			this.autopb.selected = pbAuto.length == 1;
			var atAuto = autos.filter((e:Automation) -> (e.automationType == AT));
			
			this.autoat.selected = atAuto.length == 1;
			updateCount();
			// super.populate(null);
		} catch (e) {
			throw 'Look like the wrong type was provided for Automations ${e.message}';
		}
	}

	function validate(?force=false):Bool {
		
		var errors:Array<AutoErrors> = [];
		var validTables = CC_AUTO.validate(force) && NRPN_AUTO.validate(force);
		var t = CC_AUTO.data.concat(NRPN_AUTO.data);
		if (autopb.selected)
			t.push(new Automation(PB,"",""));
		if (autoat.selected)
			t.push(new Automation(AT,"",""));
		var i = 0;
		var v = "";
		try {
			if (autocv1.selected) {
				i = 1;
				v = autocv1defo.text;
				t.push(new Automation(CV, "1", v));
			}
			if (autocv2.selected) {
				i = 2;
				v = autocv2defo.text;
				t.push(new Automation(CV, "2", v));
			}
			if (autocv1.selected) {
				i = 3;
				v = autocv3defo.text;
				t.push(new Automation(CV, "3", v));
			}
			if (autocv1.selected) {
				i = 4;
				v = autocv4defo.text;
				t.push(new Automation(CV, "4", v));
			}
		} catch (e:Exception) {
			errors.push({
				row: i,
				message: e.message,
				value: v,
				typ: CV
			});
		}
		data = t;
		if (errors.length > 0) {
			for (i in errors) {
				Notificator.dispatchErrorNotif("AUTOMATIONS error", '${i.typ} ${i.row}', '${ERRORS.get(CV)}\n\nCurrent value:"${i.value}"');
			}
			return false;
		} else if (!validTables) {
			Notificator.dispatchErrorNotif("AUTOMATIONS error",'Error with the CC or NRPN tables', '');
		} else if (data.length > 0 && hasChanges) {
			Notificator.dispatchNoErorNotification("AUTOMATIONS");
		}
		hasChanges = false;
		return true;
	}
}
