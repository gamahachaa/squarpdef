package views.windows;

import haxe.Exception;
import haxe.ui.containers.TableView;
import haxe.ui.containers.VBox;
import haxe.ui.events.ItemEvent;
import haxe.ui.notifications.NotificationManager;
import haxe.ui.notifications.NotificationType;
import signals.Signal.Signal0;
import types.Channel;
import types.DrumLane;
import types.LowRange;
import types.MidiNote;
import types.Name;
import views.components.PianorollView;

using Lambda;
using StringTools;

typedef DL = {
	var row:String;
	var trig:String;
	var notenumber:String;
	var chan:Dynamic;
	var name:String;
}

@:build(haxe.ui.ComponentBuilder.build("assets/views/windowDl.xml"))
// class WindowDrumlane extends Window {
class WindowDrumlane<T> extends VBox implements IValidatable<T> {
	var piano:PianorollView;
	public var justValidatedSignal:Signal0;
	public var data:Map<Int, DrumLane>;
	public var isValid:Bool;
	public var hasChanges:Bool;
	public var initState:Bool;

	public function new() {
		super();
		justValidatedSignal = new Signal0();
		piano = new PianorollView();
		data = [];
		isValid = true;
		initState = true;
		// this.DRUMLANES.onComponentEvent = onNoteClicked;
	}

	// @:bind(validateDL, MouseEvent.CLICK)
	public function onValidate(?force:Bool=false) {
		NotificationManager.instance.clearNotifications();
		isValid = validate(force);
		if(isValid) justValidatedSignal.dispatch();
	}

	function validate(?force:Bool = false) {
		 trace("DL validate",initState && !force, initState ,force);
		if (initState && !force)
			return true;
		var noErrors = true;
		data  = [];
		if (isEmpty())
			return true;
		for (i in 0...DRUMLANES.dataSource.size) {
			var lane = DRUMLANES.dataSource.get(i);
			if(isLaneEmpty(lane)) continue;
			//trace(isLaneEmpty(lane), lane.row);
			try {
				var n = lane.name.trim();
				var row = Std.parseInt(lane.row);
				var notenumber = lane.notenumber == "" ? new MidiNote("NULL"):MidiNote.FromDoubleString(lane.notenumber);
				var trig = lane.trig.trim();
				var name:Name = new Name(n == "" ? "NULL" : n);
				
				var c:String = lane.chan.text??cast(lane.chan);
				//var c:String = lane.chan?.text??lane.chan;
				var chan:Channel = new Channel(c);
				data.set(row, new DrumLane(row, new LowRange(trig == "" ? "NULL" : trig), chan, notenumber, name));
			} catch (error:Exception) {
				noErrors = false;
				NotificationManager.instance.addNotification({
					title: '${this.text} : NAME row ${i + 1}: bad value !',
					body: '${error.message}',
					type: NotificationType.Error,
					expiryMs: -1
				});
			}
		}
		//  trace(data, noErrors);
		if(!noErrors) return false;
		if (hasChanges) {
			hasChanges = false;
			NotificationManager.instance.addNotification({
				title: 'All good',
				body: 'No error found',
				expiryMs: 2000
			});
			
		}
		hasChanges = false;
		return true;
	}

	@:bind(DRUMLANES, ItemEvent.COMPONENT_CLICK_EVENT)
	function onCompClicked(e:ItemEvent) {
		trace("onCompClicked");
		hasChanges = true;
		initState = false;
		switch (e.source.id) {
			case "notenumber":
				{
					piano.noteSignal.add(onPianoChanged);
					piano.onNoteRequest(e.itemIndex, this.DRUMLANES.dataSource.get(e.itemIndex));
				}
			case _:
				return;
		}
	}

	function onPianoChanged(index:UInt, e:Pitch) {
		piano.noteSignal.remove(true);
		var item = this.DRUMLANES.dataSource.get(index);
		if (e == null) {
			item.notenumber = "";
		} else {
			item.notenumber = MidiNote.PitchToString(e);
		}
		DRUMLANES.dataSource.update(index, item);
	}

	public function populate(arg:Array<T>) {
		initState = true;
		try {
			// reset();
			this.DRUMLANES.dataSource.clear();
			for (i in 0...8) {
				try {
					
					var v:T = arg[i];
					this.DRUMLANES.dataSource.add(convertFrom(cast(v)));
					

				} catch (e) {
					
					this.DRUMLANES.dataSource.add({
						name: "",
						row: Std.string(i+1),
						trig: "",
						chan: "NULL",
						notenumber: ""
					});
				}
			}
			
		} catch (e) {
			throw 'Look like the wrong type was provided for Drum lanes collections';
		}
	}
	function convertFrom(d:DrumLane):DL
	{
		return {
			name: d.name,
			row: Std.string(d.row),
			trig: d.trig == "NULL" ? "" : d.trig,
			chan: Channel.CHANs.find((e:Chan) -> (e.hapx == d.chan)).verbose,
			notenumber: MidiNote.FromNumericalToPitchString(d.note)
		};
	}
	public function reset() {
		//this.DRUMLANES.dataSource.clear();
		populate(null);
	}

	function isEmpty():Bool {
		for (i in 0...8) {
			var item = DRUMLANES.dataSource.get(i);
			if(item == null)continue;
			if(!isLaneEmpty(item)) return false;
		}
		return true;
	}
	function isLaneEmpty(lane:DL):Bool{
		// trace(lane);
		// trace((lane.chan?.text == "NULL" || lane.chan == "NULL") && lane.trig=="" && lane.name =="" && lane.notenumber == "");
		return (lane.chan?.text == "NULL" || lane.chan == "NULL") && lane.trig=="" && lane.name =="" && lane.notenumber == "";
	}
}
