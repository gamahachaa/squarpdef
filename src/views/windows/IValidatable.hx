package views.windows;

import signals.Signal.Signal0;

interface IValidatable<T> {
   
    public function onValidate(?force:Bool=false):Void;
    public function reset():Void;
    public function populate(arg:Array<T>):Void;
    public var isValid:Bool;
    public var hasChanges:Bool;
    public var initState:Bool;
    public var justValidatedSignal:Signal0;
}