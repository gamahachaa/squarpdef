package views.windows;

import haxe.Exception;
import haxe.ui.components.Button;
import haxe.ui.components.CheckBox;
import haxe.ui.components.DropDown;
import haxe.ui.components.OptionStepper;
import haxe.ui.components.TextField;
import haxe.ui.containers.Frame;
import haxe.ui.containers.HBox;
import haxe.ui.core.Component;
import haxe.ui.events.MouseEvent;
import haxe.ui.events.UIEvent;
import haxe.ui.notifications.NotificationManager;
import haxe.ui.notifications.NotificationType;
import signals.Signal.Signal0;
import signals.Signal2;
import types.Control;
import types.HapaxEnums.Pot;
// import types.LowRange.CCvalue;
import types.PotAssign;
import types.PotNumber;
import types.Regex;
import utils.Converters;

using Lambda;

@:build(haxe.ui.ComponentBuilder.build("assets/views/pots.xml"))
class WindowPots extends HBox implements IValidatable<PotAssign> implements ITransferable {
	static final IDs:Array<String> = ["NULL", "CC", "PB", "AT", "CV", "NRPN"];

	public var needControlData:Signal2<ITransferable, Pot>;
	public var data:Map<PotNumber, PotAssign>;
	public var isValid:Bool;
	public var hasChanges:Bool;
	public var initState:Bool;
	public var justValidatedSignal:Signal0;

	var all:Map<String, POTS>;

	var temp:Map<PotNumber, PotAssign>;
	var closed:Bool;

	public var selectedIndex:String;

	public function new() {
		super();
		justValidatedSignal = new Signal0();
		needControlData = new Signal2<ITransferable, Pot>();

		init();
	}

	function init() {
		isValid = true;
		initState = true;
		closed = false;
		all = buildPotCollection();
		data = new Map<PotNumber, PotAssign>();
		temp = [];
		initState = true;
	}

	function someThingChanged(e:MouseEvent) {
		initState = false;
		hasChanges = true;
	}

	function onTypeChanged(e:UIEvent) {
		var stepper = cast(e.target, OptionStepper);
		var parent = e.target.parentComponent;
		// trace("Pot Type changed", parent.parentComponent.parentComponent.id);
		hideAllValues(parent.parentComponent.parentComponent);
		var selection = stepper.selectedItem.id;
		initState = false;
		switch (selection) {
			case "CC":
				{
					var val:TextField = parent.findComponent("potValueCC", TextField);
					var def:TextField = parent.findComponent("defaultValue", TextField);
					val.disabled = false;
					val.onClick = someThingChanged;
					val.hidden = false;
					def.disabled = false;
					def.hidden = false;
					def.onClick = val.onClick = someThingChanged;
				}
			case "CV":
				{
					var val:DropDown = parent.findComponent("potValueCV", DropDown);
					var def:TextField = parent.findComponent("defaultValue", TextField);
					val.hidden = false;
					val.disabled = def.disabled = false;
					def.hidden = false;
					def.onClick = val.onClick = someThingChanged;
				};
			case "NRPN":
				{
					var val:HBox = parent.findComponent("potValueNRPN", HBox);
					var def:TextField = parent.findComponent("defaultValue", TextField);
					val.hidden = def.hidden = false;
					val.disabled = def.disabled = false;
					def.onClick = val.onClick = someThingChanged;
				};
			case "AT":
				{
					var val:TextField = parent.findComponent("potValueCC", TextField);
					var def:TextField = parent.findComponent("defaultValue", TextField);
					val.hidden = true;
					def.hidden = true;
					val.text = "";
					val.disabled = true;
					def.disabled = true;
					val.onClick = someThingChanged;
				};
			case "PB":
				{
					var val:TextField = parent.findComponent("potValueCC", TextField);
					var def:TextField = parent.findComponent("defaultValue", TextField);
					val.hidden = true;
					def.hidden = true;
					val.text = "";
					val.disabled = true;
					def.disabled = false;
					def.onClick = someThingChanged;
				};
			case _:
				{
					var val:TextField = parent.findComponent("potValueCC", TextField);
					var def:TextField = parent.findComponent("defaultValue", TextField);
					val.hidden = true;
					def.hidden = true;
					// val.text = "";
					val.disabled = true;
					def.disabled = true;
					initState = true;
				};
		}
	}

	function hideAllValues(comp:Component) {
		// trace(comp.id);
		// comp.findComponent("defaultValue",TextField).disabled = true;
		for (i in IDs) {
			if (i == "PB" || i == "AT" || i == "NULL")
				continue;
			// trace("potValue" + i);
			comp.findComponent("potValue" + i).hidden = true;
		}
	}

	public function populate(arg:Array<PotAssign>) {
		try {
			// data = arg;
			init();
			// reset();
			for (v in arg) {
				setPot(v);
			}
		} catch (e) {
			throw 'Look like the wrong type was provided for Drum lanes collections';
		}
	}

	public function setFromControl(control:Control, pot:Pot) {
		trace(control, pot, selectedIndex);
		try {
			var ass:PotAssign = new PotAssign(new PotNumber(this.selectedIndex), pot, control.id, control.defaultValue);
			setPot(ass);
			NotificationManager.instance.addNotification({
				title: "Success !",
				body: 'Succefully injected control "${control.id}" into Pot $selectedIndex',
				type: NotificationType.Success,
				expiryMs: -1
			});
		} catch (e:Exception) {
			NotificationManager.instance.addNotification({
				title: "Error transfering to Pot "+selectedIndex,
				body: e.message,
				type: NotificationType.Error,
				expiryMs: -1
			});
			trace(e.message);
		}
	}

	function setPot(potassign:PotAssign) {
		trace(potassign);
		try {
			var frame = findComponent('pot${potassign.pot}', Frame);
			var type:OptionStepper = frame.findComponent("potType", OptionStepper);

			var defo:TextField = frame.findComponent("defaultValue", TextField);

			type.selectedIndex = IDs.indexOf(Std.string(potassign.potType));
			// value.text = v.value;
			defo.text = potassign.defaultValue == "NULL" ? "" : potassign.defaultValue;
			setPotValue(frame, type.selectedIndex, potassign.value);
		} catch (e:Exception) {
			trace(e.message);
		}
	}

	function setPotValue(parent:Component, typeIndex:Int, value:String) {
		switch (typeIndex) {
			case 1:
				{
					var val = parent.findComponent("potValueCC", TextField).text = value;
				}
			case 4:
				parent.findComponent("potValueCV", DropDown).selectedIndex = Std.parseInt(value);
			case 5:
				{
					var m = Regex.nrpnValuesSimple.match(value);
					if (m) {
						parent.findComponent("potValueNPRNmsb", TextField).text = Regex.nrpnValuesSimple.matched(1);
						parent.findComponent("potValueNPRNlsb", TextField).text = Regex.nrpnValuesSimple.matched(2);
						parent.findComponent("potValueNPRNdepth", CheckBox).selected = Regex.nrpnValuesSimple.matched(3) == "14";
					} else {
						throw 'Bad NPRN value given $value';
					}
				};
			case _:
				return;
		}
	}

	function getPotTyoeFromComp(parent:Component):Pot {
		return Type.createEnum(Pot, parent.findComponent("potType", OptionStepper).selectedItem.text);
	}

	function getPotFromCompID(potID:String) {
		trace(potID);
		var parent:Component = findComponent(potID, Frame);
		// var type = parent.findComponent("potType", OptionStepper).selectedItem.text;
		var pot:Pot = getPotTyoeFromComp(parent);
		var value = switch (pot) {
			case CC: parent.findComponent("potValueCC", TextField).text;
			case CV: parent.findComponent("potValueCV", DropDown).selectedItem.text;
			case NRPN: {
					var msb = parent.findComponent("potValueNPRNmsb", TextField).text;
					var lsb = parent.findComponent("potValueNPRNlsb", TextField).text;
					var d = parent.findComponent("potValueNPRNdepth", CheckBox).selected ? "14" : "7";
					'$msb:$lsb:$d';
				};
			case _: null;
		};

		var v:String;
		var vd:String;

		try {
			v = Converters.TO_VALUE(pot, value);
		} catch (e:Exception) {
			throw 'Value ${e.message}';
		}
		try {
			var isRRPNValue = Regex.nrpnValuesSimple.match(v);
			var defo = parent.findComponent("defaultValue", TextField).text;
			vd = Converters.TO_DEFO_VALUE(pot, defo == "" || defo == "default" ? null : defo, pot == NRPN ? Regex.nrpnValuesSimple.matched(3) == "7" : null);
		} catch (e:Exception) {
			throw 'DEFAULT ${e.message}';
		}

		return new PotAssign(new PotNumber(potID.substr(3)), pot, value, vd);
	}

	function getPotValue(potId:Int):PotAssign {
		if (potId > 8 && potId < 1)
			throw "unknow pot " + potId;

		return getPotFromCompID('pot$potId');
	}

	@:keep
	public function reset() {
		for (i in all) {
			resetPot(i);
		}
	}

	function resetPot(p:POTS) {
		p.defaultValue.text = "";
		p.value.ccval.text = "";
		p.value.cvval.selectedIndex = 0;
		p.value.nrpnval.depth.selected = false;
		p.value.nrpnval.msb.text = p.value.nrpnval.msb.text = "";
		p.type.selectedIndex = 0;
	}

	// @:bind(validatePots, MouseEvent.CLICK)
	public function onValidate(?force:Bool = false) {
		NotificationManager.instance.clearNotifications();
		isValid = validate();
		if (isValid)
			justValidatedSignal.dispatch();
	}

	function validate() {
		// trace(initState);
		if (initState)
			return true;
		var noErrors = true;
		for (k in 1...9) {
			var errorStrin = "";
			try {
				try {
					var value = getPotValue(k);
					if (value.potType == NULL)
						continue;

					var pn:PotNumber = new PotNumber('$k');
					// trace("vd",vd);
					temp.set(new PotNumber('$k'), value);
				} catch (e:Exception) {
					// trace(e);
					noErrors = false;
					NotificationManager.instance.addNotification({
						title: '${this.text}: ERROR with pot $k',
						body: '${e.message}',
						type: NotificationType.Error,
						expiryMs: -1
					});
				}
			} catch (e) {
				break;
			}
		}
		data = temp;
		if (!noErrors)
			return false;
		else if (temp.count() > 0 && hasChanges) {
			NotificationManager.instance.addNotification({
				title: 'All good',
				body: 'No error found',
				expiryMs: 2000
			});
		}
		hasChanges = false;
		return true;
	}

	function buildPotCollection():Map<String, POTS> {
		trace("buildPotCollection");
		var c:Map<String, POTS> = [];
		for (k in 1...9) {
			var id = 'pot$k';
			try {
				var frame:Frame = findComponent(id, Frame);
				var pot:POTS = {
					type: frame.findComponent("potType", OptionStepper),
					value: findValues(frame),
					defaultValue: frame.findComponent("defaultValue", TextField)
				};
				var btn = frame.findComponents("transfertControl", Button);
				btn[0].id= "btn"+k;
				pot.type.onChange = onTypeChanged;
				btn[0].onClick = onMenuTarget;
				//frame.onRightClick = onMenuTarget;
				c.set(Std.string(k), pot);
			} catch (e:Exception) {
				trace(e);
			}
		}
		return c;
	}

	private function onMenuTarget(e:MouseEvent) {
		trace("onMenuTarget", e.target.id);
		selectedIndex = e.target.id.substr(3);
		try {
			// final potType:Pot = getPotTyoeFromComp(e.target);
			needControlData.dispatch(this, NULL);
		} catch (e)
			trace(e.message);
	}

	function findValues(parent:Component):POTVALUE {
		return {
			ccval: parent.findComponent("potValueCC", TextField),
			cvval: parent.findComponent("potValueCV", DropDown),
			nrpnval: findNRPN(parent)
		}
	}

	function findNRPN(parent:Component):NRPNVAL {
		return {
			msb: parent.findComponent("potValueNPRNmsb", TextField),
			lsb: parent.findComponent("potValueNPRNlsb", TextField),
			depth: parent.findComponent("potValueNPRNdepth", CheckBox),
		}
	}
}

typedef POTS = {
	var type:OptionStepper;
	var value:POTVALUE;
	var defaultValue:TextField;
}

typedef POTVALUE = {
	var ccval:TextField;
	var cvval:DropDown;
	var nrpnval:NRPNVAL;
}

typedef NRPNVAL = {
	var msb:TextField;
	var lsb:TextField;
	var depth:CheckBox;
}
