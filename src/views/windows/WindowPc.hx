package views.windows;

import haxe.Exception;
import haxe.ui.events.ItemEvent;
import haxe.ui.events.MouseEvent;
import haxe.ui.events.UIEvent;
import haxe.ui.notifications.NotificationManager;
import haxe.ui.notifications.NotificationType;
import signals.Signal.Signal0;
import types.HighRange;
import types.LowRange;
import types.Name;
import types.ProgramChange;

using StringTools;

@:build(haxe.ui.ComponentBuilder.build("assets/views/pc.xml"))
class WindowPc<T> extends BaseInteractiveTables<T> {
	public var data:Map<Name, ProgramChange>;
	public function new() {
		super();
		initState = true;
		getComps();
		data = new Map<Name, ProgramChange>();
	}

	@:bind(add, MouseEvent.CLICK)
	function onAddItemInTable(e:MouseEvent) {
		addItemInTable({
			pc: "",
			msb: "",
			lsb: "",
			name: ""
		});
	}

	@:bind(PC, ItemEvent.COMPONENT_CLICK_EVENT)
	override function onDeleteTableItem(e:ItemEvent) {
		super.onDeleteTableItem(e);
	}

	@:bind(tableView, UIEvent.CHANGE)
	function onTableChange(e:UIEvent) {
		trace("PC table changed", e.target.id);
	}

	override public function populate(arg:Array<T>) {
		try {
			for (i in arg) {
				this.PC.dataSource.add(i);
			}
			super.populate(null);
		} catch (e) {
			throw 'Look like the wrong type was provided for PC collections';
		}

		// throw new haxe.exceptions.NotImplementedException();
	}


	override function validate(?force:Bool=false):Bool {
		if (super.validate() && !force)
			return true;
		else {
			var noErrors = true;
			var pcs:Map<Name, ProgramChange> = [];
			if (PC.dataSource != null) {
				for (i in 0...PC.dataSource.size) {
					var pc = PC.dataSource.get(i);
					var pcNAme = pc.name.trim();
					try {
						if(pcNAme == "" || pcNAme == null) throw "Why create a Program change withtout a name ?!? ";
						var name:Name = new Name(pcNAme);
						var pcNum:HighRange = new HighRange(pc.pc);
						var pcMsb:LowRange = new LowRange(pc.msb == "" ? "NULL" : pc.msb);
						var pcLsb:LowRange = new LowRange(pc.lsb == "" ? "NULL" : pc.lsb);
						var p = new ProgramChange(name, pcNum, pcMsb, pcLsb);
						pcs.set(name, p);
					} catch (e:Exception) {
						noErrors = false;
						NotificationManager.instance.addNotification({
							title: '${this.text} on row ${i + 1}: bad value!',
							body: '${e.message}',
							type: NotificationType.Error,
							expiryMs: -1
						});
					}
				}
			}
			data = pcs;
			if(!noErrors)return false;
			if (noErrors && hasChanges) {
				if (tableView.dataSource.size > 0)
					dispatchNoErorNotification();
			}
			hasChanges = false;
			return true;
		}
	}
}
