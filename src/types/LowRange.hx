package types;
import haxe.Exception;
import utils.Converters;

using StringTools;
abstract LowRange(String) to String{
    public function new(?v:String) {
        if(v=="NULL") this = v;
        else if(!Converters.IS_BETWEEN(v,0,128)) throw new Exception('Non valid value "$v" (from 0 to 127 or NULL)');
        else this = v;
    }
}
abstract CCvalue(String) to String{
        public function new(?v:String) {
        if(!Converters.IS_BETWEEN(v,0,120)) throw new Exception('Non valid value "$v" (from 0 to 119)');
        this = v;
    }
}