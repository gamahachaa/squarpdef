package types;
using utils.StringToolsBB;
class DrumLane {
    public static final TAG = "DRUMLANES";
    //public static var all:Map<UInt,DrumLane>=[];
    public var row:UInt;
    public var trig:LowRange;
    public var chan:Channel;
    public var note:MidiNote;
    public var name:Name;
    public function new(row:UInt,trig:LowRange,chan:Channel,note:MidiNote,name:Name) {
        if(row > 8 || row < 1) throw '$row must be between 1 and 8';
        this.row = row;
        this.trig = trig;
        this.chan = chan;
        this.note = note;
        this.name = name;
    }

    @keep
    public function toString():String{
        var t = trig == null ? "NULL":Std.string(trig);
        return '$row:$t:${chan.leadingZeros(2)}:$note $name';
    }

}
