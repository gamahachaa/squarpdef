package types;
import haxe.Exception;
abstract Binary128(String) to String{
    public function new(?n:String) {
       var t:Array<String> = [for (i in 0...16384) Std.string(i)];
       t.push("NULL");
       if(n != null && t.indexOf(n)==-1)
           throw new Exception('$n is not an unsigned Int in the 0...16383 range');
       this = n;
    }
}
