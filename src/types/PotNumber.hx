package types;
abstract PotNumber(String) to String{
    static var T = [for (i in 1...9) Std.string(i)];
    public function new(nb:String) {
        //var t = [for (i in 1...9) Std.string(i)];
        if(T.indexOf(nb) >-1) this = nb;
        else throw 'Pots are from 1 to 8, $nb doesn\'t exists';
    }
}
