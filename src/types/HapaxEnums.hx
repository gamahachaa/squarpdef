package types;


enum Pot {
    CC;
    PB;
    AT;
    CV;
    NRPN;
    NULL;
}
//enum TrackType{
//    POLY;
//    DRUM;
//    MPE;
//    NULL;
//}
//enum FirstMidiPorts{
//    A;
//    B;
//}
//enum SecondMidiPorts{
//    C;
//    D;
//}
//enum Outport{
//    NULL;
//    A;
//    B;
//
//    C;
//    D;
//    USBD;
//    USBH;
//
//    CV1;
//    CV2;
//    CV3;
//    CV4;
//    CVG1;
//    CVG2;
//    CVG3;
//    CVG4;
//    G1;
//    G2;
//    G3;
//    G4;
//}
//enum CVG{
//    CVG1;
//    CVG2;
//    CVG3;
//    CVG4;
//}
//enum CV{
//    CV1;
//    CV2;
//    CV3;
//    CV4;
//}
//enum Gate{
//    G1;
//    G2;
//    G3;
//    G4;
//}
//enum Inport{
//    NONE;
//    ALLACTIVE;
//    A;
//    B;
//    USBD;
//    USBH;
//    CVG;
//}
//enum Usb{
//    USBD;
//    USBH;
//}


class HapaxEnums {
    static public var Usb:Array<String> = [
        "USBD",
        "USBH"
    ];
    static public var Inport:Array<String> = [
        "NULL",
        "NONE",
        "ALLACTIVE",
        "A",
        "B",
        "USBD",
        "USBH",
        "CVG"
    ];
    static public var Gate:Array<String> = [
        "G1",
        "G2",
        "G3",
        "G4"
    ];
    static public var CV:Array<String> = [
        "CV1",
        "CV2",
        "CV3",
        "CV4"
    ];
    static public var CVG:Array<String> = [
        "CVG1",
        "CVG2",
        "CVG3",
        "CVG4"
    ];
    static public var Outport:Array<String> = [
        "NULL",
        "A",
        "B",
        "C",
        "D",
        "USBD",
        "USBH",
        "CV1",
        "CV2",
        "CV3",
        "CV4",
        "CVG1",
        "CVG2",
        "CVG3",
        "CVG4",
        "G1",
        "G2",
        "G3",
        "G4"
    ];
    static public var OutportMidi:Array<String> = [
        "A",
        "B",
        "C",
        "D",
        "USBD",
        "USBH"
    ];
    static public var TrackType:Array<String> = [
        "NULL",
        "POLY",
        "DRUM",
        "MPE",
    ];
    static public var Pot:Array<String> = [
        "NULL",
        "CC",
        "PB",
        "AT",
        "CV",
        "NRPN"   
    ];
    static var ALL:Map<String, Array<String>> =[];
    static public function POPULATE_ALL(){
        var all_fields = Type.getClassFields(HapaxEnums);
        for(i in all_fields){
            if(i == "ALL" || i == "POPULATE_ALL") continue;
            //Reflect.field(HapaxEnums,i );
            ALL.set(i, Reflect.field(HapaxEnums,i ));
        }
    }
    static public function IS_midi_outport_index(index:Int){
        return OutportMidi.indexOf(Outport[index]) > -1;
    }
    public function new() {
    }
}
