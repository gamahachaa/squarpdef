package types;
import haxe.Exception;

using Lambda;
using utils.StringToolsBB;
typedef Chan ={
    var hapx:String;
    var verbose:String;
 }
abstract Channel(String) to String{
    public static var CHANs:Array<Chan>= [
    {verbose:"NULL",hapx:"NULL"},
    {verbose:"ALL",hapx:"ALL"},
        {verbose:"Midi 1",hapx:"1"},
        {verbose:"Midi 2",hapx:"2"},
        {verbose:"Midi 3",hapx:"3"},
        {verbose:"Midi 4",hapx:"4"},
        {verbose:"Midi 5",hapx:"5"},
        {verbose:"Midi 6",hapx:"6"},
        {verbose:"Midi 7",hapx:"7"},
        {verbose:"Midi 8",hapx:"8"},
        {verbose:"Midi 9",hapx:"9"},
        {verbose:"Midi 10",hapx:"10"},
        {verbose:"Midi 11",hapx:"11"},
        {verbose:"Midi 12",hapx:"12"},
        {verbose:"Midi 13",hapx:"13"},
        {verbose:"Midi 14",hapx:"14"},
        {verbose:"Midi 15",hapx:"15"},
        {verbose:"Midi 16",hapx:"16"},
        {verbose:"Gate 1",hapx:"G1"},
        {verbose:"Gate 2",hapx:"G2"},
        {verbose:"Gate 3",hapx:"G3"},
        {verbose:"Gate 4",hapx:"G4"},
        {verbose:"CV 1",hapx:"CV1"},
        {verbose:"CV 2",hapx:"CV2"},
        {verbose:"CV 3",hapx:"CV3"},
        {verbose:"CV 4",hapx:"CV4"},
        {verbose:"CvGate 1",hapx:"CVG1"},
        {verbose:"CvGate 2",hapx:"CVG2"},
        {verbose:"CvGate 3",hapx:"CVG3"},
        {verbose:"CvGate 4",hapx:"CVG4"}
     ];
    static public var ALL:Array<String> =["NULL","ALL"].concat([for (i in 1...17) Std.string(i)]);
    public function new(channel:String) {
        var verboseIndex = CHANs.findIndex((e)->(e.verbose==channel));
        var hapaxIndex = CHANs.findIndex((e)->(e.hapx==channel));
        if (hapaxIndex == -1 && verboseIndex == -1 ) throw new Exception('Wrong channel "$channel"');
        else{
            this = verboseIndex>-1 ? CHANs[verboseIndex].hapx : channel;
        }
        
    }
}