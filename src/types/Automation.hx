package types;
import types.HapaxEnums;
import types.LowRange;
import utils.Converters;
class Automation {
    static public inline var TAG:String = "AUTOMATION";
    public static final MAX:Int=64;
    //static public var all:Map<Int,Automation> =[];
    public var automationType:Pot;
    public var _value:String;
    public var defaultValue:String;


    public function new(type:Pot, value:String, ?defo:String="") {
        if(type == NULL) throw 'Automation type cannot be null';
        this.automationType = type;
        this.defaultValue = defo==""?"":new LowRange(defo);
        this._value = Converters.TO_VALUETYPE(type, value);
    }
    public function toString():String{
        // var s = '$automationType';
        return automationType + (_value==null ?"": ':$_value') + (defaultValue == null ?'':':DEFAULT=$defaultValue');
    }
}