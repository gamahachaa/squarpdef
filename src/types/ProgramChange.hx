package types;
class ProgramChange {
    public var pc:HighRange;
    public  var msb:LowRange;
    public var lsb:LowRange;
    public var name:Name;
    public static final TAG = "PC";
   //public static var all:Map<Name, ProgramChange >= [];
    public function new(name:Name, pc:HighRange, ?msb:LowRange, ?lsb:LowRange) {
        this.pc = pc;
        this.msb = msb;
        this.lsb = lsb;
        this.name = name;
       // all.set(name, this);
    }
    @keep
    public function toString():String{
        var m = this.msb==null?"NULL":Std.string(this.msb);
        var l = this.lsb==null?"NULL":Std.string(this.lsb);
        return '$pc${msb==null && lsb==null?'':':$m:$l'} $name';
    }
}
