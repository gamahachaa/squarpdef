package types;
import haxe.Exception;
import utils.Converters;
abstract HighRange(String) to String{
    public function new(?v:String) {        
        if(v=="NULL") this = v;
        else if(!Converters.IS_BETWEEN(v,1,129)) throw new Exception('Non valid number "$v" (from 1 to 128 or NULL)');
        else this = v;

    }
}
