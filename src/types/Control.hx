package types;
using Lambda;

class Control {
	public var name(get, null):Name;
	public var id(get, null):String;
	public var defaultValue:String;

	public function new(name:Name, id:String, ?defaultControl:String = "NULL") {
		this.name = name;
		this.id = id;
		this.defaultValue = defaultControl;
	}

	@keep
	public function toString():String {
		return if (defaultValue == null) '$id $name'; else '$id:DEFAULT=$defaultValue $name';
	}

	function get_name():Name {
		return name;
	}

	function get_id():String {
		return id;
	}

	public static function buildAphabetTree(tab:Map<String, Control>):Map<String, Map<String, Control>> {
		var cTabs = tab.map((e)->(e.name+"#"+e.id)).array();
		cTabs.sort((a, b) -> {
			final A = a.toLowerCase();
			final B = b.toLowerCase();
			return A == B ? 0 : A > B ? 1 : -1;
		});
		// trace(cTabs);
		var m:Map<String, Map<String, Control>> = [];
		for (i in cTabs){
			var id = i.substring(i.lastIndexOf("#")+1);
			var name = i.substring(0,i.lastIndexOf("#"));
			var alphabet = name.substr(0, 1).toUpperCase();
			var c = tab.get(id);
			//trace(id, i.substring(0,i.lastIndexOf("#")));
			if (!m.exists(alphabet))
				m.set(alphabet, [name => c]);
			if (!m.get(alphabet).exists(name))
				m.get(alphabet).set(name, c);
		}
		// for (k => v in tab) {
		// 	var alphabet = cast(v.name, String).substr(0, 1).toUpperCase();
		// 	if (!m.exists(alphabet))
		// 		m.set(alphabet, [v.name => v]);
		// 	if (!m.get(alphabet).exists(v.name))
		// 		m.get(alphabet).set(v.name, v);
		// }
		return m;
	}
}
