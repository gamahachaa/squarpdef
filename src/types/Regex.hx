package types;
class Regex {
    public static var openTagEreg = ~/^\[([A-Z_]+)\]$/i;
    public static var closeTagEreg = ~/^\[\/([A-Z_]+)\]$/i;
    public static var commentStartLineEreg = ~/^#/;
//    public static var basicValueStartEreg = ~/^([A-Z]+)\s([^#]+)(#.+)?$/;
    public static var basicValueStartEreg = ~/^([A-Z]+)\s([^#]+)$/;
    public static final noteNumReg = ~/^(0{1,2}[1-9]|0?[1-9][0-9]?|1[01][0-9]|12[0-7]|00|000|0)$/;
	public static final noteReg = ~/^(([CDF]#?|[EG])10)$|^(([CDFGA]#?|[EB])([0-9]))$/i;
	public static final noteReg2 = ~/^([a-g]#?)(0?[0-9]{0,1}|10)$/i;
	public static final noteRegex = ~/^([CDF]#?)(-1|[0-9])|([E])(-1|[0-9])|(G)(-1|[0-9])|(G#|A|A#|B)(-1|[0-8])$/i;
	public static final noteRegexSub = ~/^([A-Z]#?)([-0-9]+)/i;
    // values in .txt files
    /**
    @todo make config file loader for easy updates
**/

    //public static var automationsValues = ~/^([A-Z]{2,4}):([0-9]{1,3})(:DEFAULT=([0-9]{1,3}))?$/;
    public static var automationsValues = ~/^(CC|PB|AT|CV|NRPN):?(([0-9]{0,3})|([0-9]{0,3}):([0-9]{0,3}|NULL):([0-9]{0,3}|NULL))?((\s|:)?DEFAULT=([0-9]{1,3}|NULL))?$/i;
    //public static var potAssignValues = ~/^([1-8])\s([A-Z]{2,4}):([0-9]{1,3})(:DEFAULT=([0-9]{1,3}))?$/;
    public static var potAssignValues = ~/^([1-8])\s(CC|PB|AT|CV|NRPN):([0-9]{1,3}:[0-9]{1,3}:[0-9]{1,3}|[0-9]{1,3})(\sDEFAULT=(-?[0-4](\.\d{1,2})?V|-?5(\.0{1,2})?V|[0-9]{1,5}))?$/i;
    public static var nrpnValues = ~/^([0-9]{0,3}):([0-9]{1,5}):(7|14)(:DEFAULT=([0-9]{1,5}))?$/i;
    public static var nrpnValuesSimple = ~/^([0-9]{0,3}):([0-9]{1,5}):(7|14)$/;
    // public static var nrpnValuesName = ~/^(([0-9]{0,3}):([0-9]{1,5}):(7|14)(:DEFAULT=([0-9]{1,5}))?)(\s([. _\-+a-z0-9]*))$/i;
    public static var nrpnValuesName = ~/^(([0-9]{0,3}):([0-9]{1,5}):(7|14)(:DEFAULT=([0-9]{1,5}))?)(\s([^#]*))/i;

    public static var ccValues = ~/^([0-9]{1,3})(:DEFAULT=([0-9]{1,3}))?$/i;
    public static var ccAssignValues = ~/^(([0-9]{1,2})|(1[0-1][0-9]))$/ig;
    public static var basicLowRangeValues = ~/^(([0-9]{1,2})|(1[0-1][0-9])|(12[0-7]))$/;
    public static var basicHighRangeValues = ~/^([1-9]|([1-9][0-9])|(0?[0-9][1-9])|(1[0-1][0-9])|(12[0-8]))$/;
    public static var ccPairsValues = ~/^([0-9]{1,3}):([0-9]{1,3})(:DEFAULT=([0-9]{1,5}))?$/i;
    //public static var ccPairsValues = ~/^([0-9]{1,3}):([0-9]{1,3})(:DEFAULT=(?:[0-9]\d{0,3}|[1-9]\d{0,3}|1[0-5]\d{3}|1638[0-3]))?$/i;
    public static var pcValues:EReg = ~/^(([0-9]{1,3}):([0-9]{1,3}|NULL):([0-9]{1,3}|NULL)|([0-9]{1,3}))$/i;
    // static inline var pcValueWithName = '^$pcValues\\s$names$';
    // public static var pcValuesReg:EReg = new EReg("^(([0-9]{1,3}):([0-9]{1,3}|NULL):([0-9]{1,3}|NULL)|([0-9]{1,3}))\\s([. _\\-+a-z0-9]*)$","i");
    public static var pcValuesReg:EReg = ~/^(([0-9]{1,3}):([0-9]{1,3}|NULL):([0-9]{1,3}|NULL)|([0-9]{1,3}))\s([. _\-+a-z0-9]*)$/i;
    static inline var names = '([. _\\-+a-z0-9]*)';
    @:keep
    public static var hapaxVersionLine =~/^VERSION\s([0-9]+(\.[0-9]{1,2})?)/i;

    public static var version:EReg = ~/([0-9]+(\.[0-9]{1,2})?)/;
    public static var trackName: EReg = ~/^([. _\-+a-z0-9]+)$/i;
    public static var trackNameLine: EReg = ~/^TRACKNAME\s([. _\-+a-z0-9]*)$/i;
    public static var ccName: EReg = ~/^([^#]*)$/i;
    public static var nrpnEreg = ~/^([0-9]|[1-9][0-9]|1[01][0-9]|12[0-7]):([0-9]|[1-9][0-9]|1[01][0-9]|12[0-7]):(7|14)$/i;
    public static var voltageValues:EReg = ~/^-?[0-4](\.\d{1,2})?V|-?5(\.0{1,2})?V$/gm;
    public static var cv65535 :EReg = ~/^(0|[1-9]\d?|1\d{2}|2[0-4]\d|25[0-5]|3[0-4]\d{2}|35[0-1]\d|352[0-7]|4\d{3}|5[0-4]\d{2}|55[0-2]\d|553[0-5]|6[0-4]\d{3}|65[0-4]\d{2}|655[0-2]\d|6553[0-5])$/;
    public static var pyraKeyValue:EReg = ~/^(\S+)+:([^#]*)$/i;
    @:keep
    public static var pyraKeyName:EReg = ~/^NAME:(\S+)/i;
    public static var pyraKeyNote:EReg = ~/^N([0-9]{1,3})$/i;
    public static var pyraCCkey:EReg = ~/^[0-9]{1,3}$/i;
    public static var pyraOut:EReg = ~/^OUT:(A|B|USB)$/i;
   public static var urlReserved:EReg = ~/[ !\*'\(\);:@&=\+\$,\/\?%#\[\]]+/i;
   // drumlane 1:2:6:11 16 
   public static var drumlaneReg:EReg = ~/^([1-8]):(NULL|(12[0-7])|(1[0-1][0-9])|([0-9]{1,2})):(NULL|(1[0-6]|[0]?[1-9])|(G[1-4])|(CV[1-4])|(CVG[1-4])):(NULL|(12[0-7])|(1[0-1][0-9])|([0-9]{1,2}))(\s([^#]*))?/i;
    public static var testEmbed:String = "hello|WORLD";


}
//All other Question categories so that only one is default