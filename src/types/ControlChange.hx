package types;
typedef CC={
    var name:Name;
    var cc:LowRange;
    var defaultControl:String;
}
class ControlChange extends Control{
    public static final TAG = "CC";
    public static final BLANK =  new ControlChange(new Name("BLANK CC"), new LowRange("0"), "NULL");
    @:isVar public var cc(get, null):LowRange;

    function get_cc():LowRange {
        return cc;
    }


    public function new(name:Name,cc:LowRange,?def:String) {
        if(name == null) throw 'CC must have a name';
        if(cc == null || cc == "" || cc == "NULL") throw 'CC must have a value';
        super(name, cc, def==null || def==""?"":new LowRange(def));
        this.cc = cc;

    }

}
