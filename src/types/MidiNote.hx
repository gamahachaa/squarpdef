package types;
import haxe.Exception;
import types.Regex;
abstract MidiNote(String) to String {
	static final PITCH_SEP = " - ";
    public static var NOTES:Array<String>=[
        "C",
        "C#",
        "D",
        "D#",
        "E",
        "F",
        "F#",
        "G",
        "G#",
        "A",
        "A#",
        "B"
    ];
    public static var NUMBERS:Array<String> = [for (i in 0...128) Std.string(i)].concat(["NULL"]);
    public function new(?note:String) {
       // var t = [for (i in 0...128) Std.string(i)];
        //t.push("NULL");
        if(note!=null && NUMBERS.indexOf(note)== -1) throw new Exception('Non valid note number "$note" (from 0 to 127 or NULL)');
        this = note;
    }
	public static function FromDoubleString(notation:String):MidiNote{
		return new MidiNote(StringToPitch(notation).numerical);
	}
    public static inline function fromMusicalNotation(s:String):MidiNote
    {
        //var noteRegex:EReg = ~/^([CDF]#?)(-1|[0-9])|([E])(-1|[0-9])|(G)(-1|[0-9])|(G#|A|A#|B)(-1|[0-8])$/i;
        //var subRegex:EReg = ~/^([A-Z]#?)([-0-9]+)/i;
        var matched = Regex.noteReg.match(s);
        return if(matched){
            Regex.noteRegexSub.match(s);
            var letter =  Regex.noteRegexSub.matched(1);
            var num = Std.parseInt(Regex.noteRegexSub.matched(2));
            new MidiNote( Std.string((num + 1)*12) + NOTES.indexOf(letter) );
        }
        else{
            throw new Exception('Non valid note number "$s" (from C0 to G9)');
        }
    }
	public static inline function FromNumericalToPitchString(num:String):String {
		return PitchToString(FromNumerical(num));
	}
    public static inline function FromNumerical(num:String):Pitch {
		return ObjectFromValues(num);
	}

	public static inline function FromMusical(note:String):Pitch {
		return ObjectFromValues("", note);
	}

	public static inline function ObjectFromValues(?num:String = "", ?note:String = ""):Pitch {
		if ((num == null || num == "") && note == "")
			throw new Exception("No note value given");
		else {
			if (num != "") {
				if (Regex.noteNumReg.match(Std.string(num))) {
					return {
						numerical: new MidiNote(num),
						musical: GETMusicalNoteFromNumerical(num)
					}
				} else
					throw new Exception('Bad midi note number given : "$num" (should be beteween 0 and 127)');
			} else {
				if (Regex.noteReg.match(note)) {
					return {
						musical: note,
						numerical: fromMusicalNotation(note)
						//numerical: GETNumericalFromMusical(note)
					}
				} else {
					throw new Exception('Bad midi note given : "$note" (should be beteween C0 and G10)');
				}
			}
		}
	}
	public static inline function GETNumericalFromMusical(note:String):MidiNote {
		if (Regex.noteReg.match(note) && ["G#10", "A10", "A#10", "B10", "B#10"].indexOf(note.toLowerCase()) == -1)
			return GETNumericalNoteFromMusicalPair(Regex.noteReg.matched(1), Regex.noteReg.matched(2));
		else
			throw new Exception('Bad midi note given : "$note" (should be beteween C0 and G10)');
	}
	
    public static inline function GETNumericalNoteFromMusicalPair(note:String, octave:String):MidiNote {
		return new MidiNote(Std.string(NOTES.indexOf(note.toUpperCase()) + (Std.parseInt(octave) * 12)));
	}
	public static inline function GETMusicalNoteFromNumerical(num:String) {
		final noteAndOctave = GETNoteAndOctaveFromNum(num);
		return (noteAndOctave.note + Std.string(noteAndOctave.octave)).toUpperCase();
	}
	public static inline function GETNoteAndOctaveFromNum(num:String) {
        final n = Std.parseInt(num);
		return {
			octave: Math.floor(n / 12),
			note: NOTES[n % 12]
		}
	}
	public inline static function PitchToString(p:Pitch){
		return p.numerical + PITCH_SEP + p.musical;
	}
	public static function StringToPitch(s:String){
		final tmp = s.split(PITCH_SEP);
		return {
			musical: tmp[1],
			numerical: tmp[0]
		}
	}
	
}
typedef Pitch = {
	var musical:String;
	var numerical:MidiNote;
}