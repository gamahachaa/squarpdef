package types;
using StringTools;

typedef CCP = {
    var name:Name;
    var msb:LowRange;
    var lsb:LowRange;
    var defaultControl:String;
}
class ControlChangePair extends Control{
    //public var id:String;
    public var msb:LowRange;
    public var lsb:LowRange;
   // public var def:Binary128;
    //public static var all:Map<String, ControlChangePair> = [];
    public inline static var TAG:String = "CC_PAIR";
    public function new(name:Name,msb:LowRange,lsb:LowRange,?defo:String ) {
       
        if(name == null) throw 'CC pair must have a name';
        if(msb == null || msb == "" || msb == "NULL" || lsb == null || lsb == "" || lsb == "NULL") throw 'CC pairs msb and lsb must have a value';
        
        super(name, 
            '$msb:$lsb', 
            defo==null || defo=="NULL" || defo=="" ?"":new Binary128(defo.trim())
            );
       
        this.msb = msb;
        this.lsb = lsb;
    }
}
