package types;
using StringTools;
abstract Name(String) to String {
    public function new(name:String, ?strict:Bool=true) {
        var n = name.trim();
        if(!Regex.trackName.match(n)&&strict || !Regex.ccName.match(n)&&!strict){
            throw '"$n" ...\nNames supports all alphanumeric ASCII, and spaces, _ , - , + , - ,';
        }
        // trace("ok",n);
        this = n;
    }
}

