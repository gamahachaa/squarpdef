package types;

import types.HapaxEnums;
import utils.Converters;
class PotAssign {
    public static inline var TAG:String = "ASSIGN";
    //public static var all:Map<PotNumber, PotAssign> = [];
    public var pot:PotNumber;
    public  var potType:Pot;
    public var value:String;
    public var defaultValue:String;

    public function new(pot:PotNumber,type:Pot, value:String, ?defo:String) {
        
        this.pot = pot;
        this.potType = type;
        this.value = Converters.TO_VALUETYPE(type,value);
        
        var depth = if(type==NRPN && Regex.nrpnEreg.match(this.value)){
            Regex.nrpnEreg.matched(3)=="7";
        }else true;
        //
        this.defaultValue = defo==null?defo:Converters.TO_DEFO_VALUE(type,defo,depth);
       
    }
    public function toString():String{
        var s = if(potType == PB || potType == AT)
            '$pot $potType';
            else '$pot $potType:$value';
        if(defaultValue != null)
            s + ' DEFAULT=$defaultValue';
        return s;
    }
}
