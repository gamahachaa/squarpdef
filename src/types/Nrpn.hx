package types;

import haxe.ds.Either;
import types.LowRange;
import utils.Converters;

typedef NRPNS = {
	var name:Name;
	var msb:LowRange;
	var lsb:String;
	var depth:Bool;
	var defaultControl:String;
}

class Nrpn extends Control{
	public static final TAG:String = "NRPN";
	public static final BLANK = new Nrpn(new Name("BLANK NRPN"), false, Right(new LowRange("0")),new LowRange("0"), "NULL");
	// public static var all:Map<String,Nrpn> = [];
	// public var name:Name;
	public var msb:LowRange;
	public var lsb:String;
	public var depth:Bool;
	// public var def:String;
	// public var id(get, default):String;

	public function new(name:Name, 
		depth14:Bool, 
		lsb:Either<Binary128, LowRange>, 
		?msb:LowRange, 
		?defo:String) {
		
		if(name == null) throw 'NRPN must have a name';
		this.depth = depth14;
		this.msb = msb;
		var msbIsNull = msb == null || msb == "0" || msb == "NULL";
		switch (lsb) {
			case Right(v):
				this.lsb = v;
			case Left(v):
				{
					var lsbAbove = Std.parseInt(v) > 127;
					if (lsbAbove && msbIsNull || !lsbAbove)
						this.lsb = v
					else
						throw '$v ... LSB can be over 127 ONLY if MSB is 0 or omitted';
					
				}
		}
		var m:String = msb == null ? '' : Std.string(msb);
		id = '$m:${this.lsb}:${depth ?  "14": "7"}';
		super(name, id, Converters.TO_DEFO_VALUE(NRPN, defo, !depth));
	}
}
