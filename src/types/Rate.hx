package types;
abstract Rate(String) to String{
    public static var RATE:Array<String>=[
        "1",
        "2",
        "3",
        "4",
        "6",
        "8",
        "12",
        "24",
        "32",
        "48",
        "64",
        "96",
        "192",
        "NULL"
    ];
     public function new(?rate:String="NULL") {
        if (RATE.indexOf(rate.toUpperCase()) == -1) throw 'Unknown rate "$rate"';
        this = rate;
    }
}
