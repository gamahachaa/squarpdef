VERSION 1 # Currently, this should only be 1.

# Supports all alphanumeric ASCII, and ' ', '_', '-', '+' - can also be NULL
TRACKNAME COBALT

# Can be POLY, DRUM, MPE, or NULL
TYPE POLY

# Can be A, B, C, D, USBD, USBH, CVGx (x between 1&4), CVx, Gx, or NULL
OUTPORT B

# Can be x (between 1-16), or NULL -- this is ignored if output port is not MIDI
OUTCHAN 1

# Can be NONE, ALLACTIVE, A, B, USBH, USBD, CVG, or NULL
INPORT ALLACTIVE

# Can be x (between 1-16), ALL, or NULL. This definition will be ignored if INPORT is NONE, ALLACTIVE or CVG
INCHAN NULL

# This defines the default 'MAX RATE' for automation lanes.
# Can be NULL, 192, 96, 64, 48, 32, 24, 16, 12, 8, 6, 4, 3, 2, 1
MAXRATE NULL

# DRUMLANES
# Syntax: ROW:TRIG:CHAN:NOTENUMBER NAME
# ROW must be between 1 and 8
# TRIG can be between 0 and 127, or NULL
# CHAN can be a number between 1 and 16, Gx, CVx, CVGx (x between 1 and 4), or NULL
# NOTENUMBER can be between 0 and 127, or NULL
# NAME supports all alphanumeric ASCII, and ' ', '_', '-', '+' - can also be NULL
# Please note this section will be discarded for tracks which are not DRUM tracks
[DRUMLANES]
[/DRUMLANES]

# PC
# Syntax: NUMBER NAME
# number must be either:
#   - A number (for simple PC)
#   - Three numbers, delimited by ':', which represent PC:MSB:LSB. You can put 'NULL' to not set the MSB/LSB.
# PC must be between 1...128
# MSB/LSB must be between 0...127
[PC]
[/PC]

# CC
# Syntax: CC_NUMBER NAME or CC_NUMBER:DEFAULT=xx NAME
# DEFAULT_VALUE must be a valid number between 0 and 127
[CC]
0 Bank Change
1 Mod Wheel
5 Glide
7 Main Volume
9 Voice Mode
11 Touchpad Z
14 Osc1 Tune
15 Osc1 Fine
16 Osc1 Param A
17 Osc2 Param A
18 Osc Mix
19 Osc1 Param B
20 Osc2 Param B
22 FEG Attack
23 FEG Decay
24 FEG Sustain
25 FEG Release
26 AEG Attack
27 AEG Decay
28 AEG Sustain
29 AEG Release
30 Osc2 Tune
31 Osc2 Fine
32 FEG Amount
33 Morph
34 Cutoff
35 Resonance
36 LFO1 Rate
37 LFO1 Depth
39 LFO1 Shape
40 Octave
42 Filter Type
43 MEG Attack
44 MEG Decay
45 MEG Sustain
46 MEG Release
47 LFO2 Rate
48 LFO2 Depth
49 MEG Depth
50 LFO2 Shape
51 AEG Depth
52 LFO MIDI Sync
53 Arp Gate
55 Delay MIDI Sync
56 LFO1 Mode
57 LFO2 Mode
58 Arp Status
59 Arp Octave
60 Arp Direction
61 Arp Division
62 Osc1 Algorithm
63 Osc2 Algorithm
64 Sustain Pedal
67 Seq Length
68 Seq Mute
69 Seq Clear
70 Seq Hold
71 Seq Loop
72 Seq Record
75 Transpose
78 Swing
79 Seq Quantise
80 AEG Type
81 FEG Type
82 MEG Type
84 Attack
85 Decay
86 Sustain
87 Release
88 ModSlot 1 Depth
89 ModSlot 2 Depth
90 ModSlot 3 Depth
91 ModSlot 4 Depth
92 ModSlot 5 Depth
93 ModSlot 6 Depth
94 ModSlot 7 Depth
95 ModSlot 8 Depth
100 ModSlot 1 Source
101 ModSlot 2 Source
102 ModSlot 3 Source
103 ModSlot 4 Source
104 ModSlot 5 Source
105 ModSlot 6 Source
106 ModSlot 7 Source
107 ModSlot 8 Source
108 ModSlot 1 Dest
109 ModSlot 2 Dest
110 ModSlot 3 Dest
111 ModSlot 4 Dest
112 ModSlot 5 Dest
113 ModSlot 6 Dest
114 ModSlot 7 Dest
115 ModSlot 8 Dest
116 Touchpad X+
117 Touchpad X-
118 Touchpad Y-
119 Patch Gain
120 All Sound Off
121 Reset All Controllers
123 All Notes Off
124 Omni Off
125 Omni On
[/CC]

# NRPN
# Syntax: "MSB:LSB:DEPTH NAME" or "MSB:LSB:DEPTH:DEFAULT=xx NAME"
# Lsb & msb should be between 0 and 127
  # Note: LSB can be over 127 ONLY if MSB is 0 or omitted
  # e.g. '0:1026:7 FOO', or ':2000:7 BAR' 
  # the examples above would be equivalent to '8:2:7 FOO' and '15:80:7 BAR' respectively
# DEPTH can be 7 or 14
# For NRPN: DEFAULT_VALUE must be a valid number, either between 0 and 127 (for 7 bit NRPNs) or between 0 and 16383 (for 14bit NRPNs)
[NRPN]
[/NRPN]

# ASSIGN
# Syntax: POT_NUMBER TYPE:VALUE or POT_NUMBER TYPE:VALUE DEFAULT=DEFAULT_VALUE
# POT_NUMBER must be between 1 and 8
# TYPE can be "CC", "PB" (pitchbend), "AT" (aftertouch), "CV", "NRPN", or "NULL" (this won't assign the pot).
# Non explicitly-defined pots will be considered "NULL"
# VALUE VALIDATION
#### For CC: Value must be a valid number between 0 and 119
#### For PB and AT, any text after the TYPE will be ignored
#### For CV, value must be between 1 and 4
#### For NRPN, value must be MSB:LSB:DEPTH, with both lsb & msb bebtween 0 and 127, and DEPTH being either 7 or 14
# DEFAULT VALUE
#### For CC: DEFAULT_VALUE must be a valid number between 0 and 127
#### For PB: DEFAULT_VALUE must be a valid number between 0 and 16383
#### For NRPN: DEFAULT_VALUE must be a valid number, either between 0 and 127 (for 7 bit NRPNs) or between 0 and 16383 (for 14bit NRPNs)
#### For CV: DEFAULT_VALUE must be either a valid number between 0 and 65535, or a voltage between -5V and 5V, e.g. "-4.25V" or "1.7V"
#### Please note default value will be ignored for PB and AT messages.
[ASSIGN]
1 CC:0    # Bank Change
2 CC:34   # Cutoff
3 CC:84   # All EG Attack
4 CC:85   # All EG Decay
5 NULL    # PC
6 CC:35   # Res
7 CC:86   # All EG Sustain
8 CC:87   # All EG Release
[/ASSIGN]

# AUTOMATION
# Syntax: TYPE:VALUE
# TYPE can be "CC", "PB" (pitchbend), "AT" (aftertouch), "CV", or "NRPN"
# VALUE VALIDATION
#### For CC: Value must be a valid number between 0 and 119
#### For PB and AT, any text after the TYPE will be ignored
#### For CV, value must be between 1 and 4
#### For NRPN, value must be MSB:LSB:DEPTH, with both lsb & msb bebtween 0 and 127, and DEPTH being either 7 or 14
# NOTE: You are limited to 64 automation lanes
[AUTOMATION]
[/AUTOMATION]

# This section will be readable from Hapax.
[COMMENT]
Modal Cobalt 5s
[/COMMENT]