##### Nord wave 2 ######
VERSION 1 # Currently, this should only be 1.

# Supports all alphanumeric ASCII, and ' ', '_', '-', '+' - can also be NULL
TRACKNAME NORDWAVE2
# Can be POLY, DRUM, MPE, or NULL
TYPE POLY

# Can be A, B, C, D, USBD, USBH, CVGx (x between 1&4), CVx, Gx, or NULL
OUTPORT A

# Can be x (between 1-16), or NULL -- this is ignored if output port is not MIDI
OUTCHAN 7

# Can be NONE, ALLACTIVE, A, B, USBH, USBD, CVG, or NULL
INPORT NULL

# Can be x (between 1-16), ALL, or NULL. This definition will be ignored if INPORT is NONE, ALLACTIVE or CVG
INCHAN NULL

# This defines the default 'MAX RATE' for automation lanes.
# Can be NULL, 192, 96, 64, 48, 32, 24, 16, 12, 8, 6, 4, 3, 2, 1
MAXRATE NULL

# DRUMLANES
# Syntax: ROW:TRIG:CHAN:NOTENUMBER NAME
# ROW must be between 1 and 8
# TRIG can be between 0 and 127, or NULL
# CHAN can be a number between 1 and 16, Gx, CVx, CVGx (x between 1 and 4), or NULL
# NOTENUMBER can be between 0 and 127, or NULL
# NAME supports all alphanumeric ASCII, and ' ', '_', '-', '+' - can also be NULL
# Please note this section will be discarded for tracks which are not DRUM tracks
[DRUMLANES]
[/DRUMLANES]


# PC
# Syntax: NUMBER NAME
# number must be either:
#   - A number (for simple PC)
#   - Three numbers, delimited by ':', which represent PC:MSB:LSB. You can put 'NULL' to not set the MSB/LSB.
# PC must be between 1...128
# MSB/LSB must be between 0...127
[PC]
[/PC]


# CC
# Syntax: CC_NUMBER NAME or CC_NUMBER:DEFAULT=xx NAME
# DEFAULT_VALUE must be a valid number between 0 and 127
[CC]
0 MSB BANK
32 LSB BANK
64 Sustain
11 Ctrl Pedal (expression)
23 KB Hold
27 KB Hold enable
26 PStick Enable
24 Octave Shift
3 Program Level
51 Vibrato
48 Glide
49 Voice Mode
74 Arp/Gate Rate
76 Arp/Gate Range/Env
72 Arp Gate Mode
75 Arp/Gate Direction
69 Arp/Gate Zig-Zag
71 Arp/Gate KB Sync
77 Arp/Gate Run
68 Arp/Gate Mst Clk
78 LFO Amount
79 LFO Rate
110 LFO Mst Clk
80 LFO Waveform 
109 LFO Destination
42 Osc Mod Env Arrack
43 Osc Mod Env Dec/Rel 
45 Osc Mod Env Amount
44 Osc Mod Env Destionation
46 Osc Mod Env Vel
47 Osc Mod Env AR mode
39 Osc Unison
37 Osc Semitones
36 Osc Fine Tune
12 Layer A Level
13 Layer B Level
14 Layer C Level
15 Layer D Level
16 Layer A Pan
17 Layer B Pan
18 Layer C Pan
19 Layer D Pan
82 Amp Env Attack
81 Amp Env Attack Transient
83 Amp Env Decay
84 Amp Env Sustain
85 Amp Env Release
86 Amp Env Velocity
52 Filter Env Attack
53 Filter Env Decay
54 Filter Env Sustain
55 Filter Env Release
63 Filter KB Track
61 Filter Drive
59 Filter Freq
60 Filter Resonance
65 Filter Type
62 Filter Env Amount
56 Filter Env Velocity
57 Filter Env Invert
58 Filter On/Off
91 Effects On/Off
87 Effects Type
89 Effects Ammount
90 Effects Rate
88 Effects Rate Mst Clk
105 EQ/Drive On/Off
101 Drive Amount
102 EQ Bass/Gain
104 EQ Treble/Freq
106 EQ Parametric On/Off
97 Delay On/Off
93 Delay Dry/Wet
94 Delay Tempo Tap/Set
96 Delay Analog On/Off
94 Delay Rate
108 Delay Feedback Effect Type
100 Delay Feedback Effect Deep
95 Delay Feedback
99 Delay Feedback Filter Type
98 Delay Ping-Pong
116 Reverb ON 
115 Reverb Type
118 Reverb Chorale
117 Reverb Bright/Dark
113 Reverb Dry/Wet
[/CC]


# NRPN
# Syntax: "MSB:LSB:DEPTH NAME" or "MSB:LSB:DEPTH:DEFAULT=xx NAME"
# Lsb & msb should be between 0 and 127
  # Note: LSB can be over 127 ONLY if MSB is 0 or omitted
  # e.g. '0:1026:7 FOO', or ':2000:7 BAR' 
  # the examples above would be equivalent to '8:2:7 FOO' and '15:80:7 BAR' respectively
# DEPTH can be 7 or 14
# For NRPN: DEFAULT_VALUE must be a valid number, either between 0 and 127 (for 7 bit NRPNs) or between 0 and 16383 (for 14bit NRPNs)
[NRPN]
[/NRPN]


# ASSIGN
# Syntax: POT_NUMBER TYPE:VALUE or POT_NUMBER TYPE:VALUE DEFAULT=DEFAULT_VALUE
# POT_NUMBER must be between 1 and 8
# TYPE can be "CC", "PB" (pitchbend), "AT" (aftertouch), "CV", "NRPN", or "NULL" (this won't assign the pot).
# Non explicitly-defined pots will be considered "NULL"
# VALUE VALIDATION
#### For CC: Value must be a valid number between 0 and 119
#### For PB and AT, any text after the TYPE will be ignored
#### For CV, value must be between 1 and 4
#### For NRPN, value must be MSB:LSB:DEPTH, with both lsb & msb bebtween 0 and 127, and DEPTH being either 7 or 14
# DEFAULT VALUE
#### For CC: DEFAULT_VALUE must be a valid number between 0 and 127
#### For PB: DEFAULT_VALUE must be a valid number between 0 and 16383
#### For NRPN: DEFAULT_VALUE must be a valid number, either between 0 and 127 (for 7 bit NRPNs) or between 0 and 16383 (for 14bit NRPNs)
#### For CV: DEFAULT_VALUE must be either a valid number between 0 and 65535, or a voltage between -5V and 5V, e.g. "-4.25V" or "1.7V"
#### Please note default value will be ignored for PB and AT messages.
[ASSIGN]
[/ASSIGN]


# AUTOMATION
# Syntax: TYPE:VALUE
# TYPE can be "CC", "PB" (pitchbend), "AT" (aftertouch), "CV", or "NRPN"
# VALUE VALIDATION
#### For CC: Value must be a valid number between 0 and 119
#### For PB and AT, any text after the TYPE will be ignored
#### For CV, value must be between 1 and 4
#### For NRPN, value must be MSB:LSB:DEPTH, with both lsb & msb bebtween 0 and 127, and DEPTH being either 7 or 14
# NOTE: You are limited to 64 automation lanes
[AUTOMATION]
[/AUTOMATION]


# This section will be readable from Hapax.
[COMMENT]
[/COMMENT]```